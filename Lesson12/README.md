# Lesson 12

#### Any question from last lesson?
String and Date

## Books
1. [Python cookbook](https://d.cxcore.net/Python/Python_Cookbook_3rd_Edition.pdf)

## String
1. ljust() vs rjust() vs center() vs [format()](https://docs.python.org/3/library/string.html#formatspec)
1. [str.join()](https://www.programiz.com/python-programming/methods/string/join) with List, Tuple, String, Dictionary, Set
1. textwrap(): print out string with structure
1. tokenize

## Date and time
1. [round](https://docs.python.org/3/library/functions.html#round)(value, ndigits)
1. Decimal: for precision but less speed
1. base 2, base 8 and base 16
1. create inf, -inf, nan

## Hw
