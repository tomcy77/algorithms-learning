class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

# implement printTree function to print a [complete binary tree]
# (https://web.cecs.pdx.edu/~sheard/course/Cs163/Doc/FullvsComplete.html#:~:text=Full%20v.s.%20Complete%20Binary%20Trees&text=A%20full%20binary%20tree%20(sometimes,as%20far%20left%20as%20possible.)
# structure to output
# for example:
# head = Node(1)
# head.left = Node(2)
# head.right = Node(3)
# head.left.left = Node(5)
# head.left.right = Node(6)
# head.right.left = Node(7)
# when calling head.printTree()
# print out
#       1
#   2       3
#5   6    7
class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data
head = Node('a')
head.left = Node('b')
head.right = Node('c')
head.left.left = Node('d')
head.left.right = Node('e')
head.right.left = Node('f')
head.right.right = Node('g')
head.left.left.left = Node('h')
head.left.left.right = Node('i')
head.left.right.left = Node('j')
head.left.right.right = Node('k')
#head.right.left.left = Node('l')
#head.right.left.right = Node('m')
#head.right.right.left = Node('n')
#head.right.right.right = Node('o')

class Tree:
    def depthCalculator(self, root):
        depth = 0
        while root:
            depth += 1
            root = root.left
        return depth

    def traversal(self, root , level, tree):
        if root is None:
            return
        if level == 1:
            tree.append(root.data)
        elif level > 1 :
            self.traversal(self, root.left , level-1, tree)
            self.traversal(self, root.right , level-1, tree)

    def Listing(self, root):
        h = self.depthCalculator(self, root)
        tree = []
        for i in range(1, h+1):
            self.traversal(self, root, i, tree)
        return tree

    def printTree(self, root):
        tree = self.Listing(self, root)
        height = self.depthCalculator(self, root)
        n = 0 #counter for while loop
        h = 0 #current level
        while n < len(tree):
            if n+1 == 2**h:#if head
                print(' '* 2**(height-h-1), end = '')
            print(tree[n], end = '')
            n += 1
            if n == (2**(h+1)-1):#if end
                print('')
                h += 1
            else:
                print(' '* (2**(height-h)-1), end = '')
print(Tree.printTree(Tree, head))
