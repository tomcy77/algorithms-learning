class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

# implement printTree function to print a [complete binary tree]
# (https://web.cecs.pdx.edu/~sheard/course/Cs163/Doc/FullvsComplete.html#:~:text=Full%20v.s.%20Complete%20Binary%20Trees&text=A%20full%20binary%20tree%20(sometimes,as%20far%20left%20as%20possible.)
# structure to output
# for example:
# head = Node(1)
# head.left = Node(2)
# head.right = Node(3)
# head.left.left = Node(5)
# head.left.right = Node(6)
# head.right.left = Node(7)
# when calling head.printTree()
# print out
#       1
#   2       3
#5   6    7
def printTree(root):
    return
