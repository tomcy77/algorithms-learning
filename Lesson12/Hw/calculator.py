# implement a simple calculator which takes a string as input
# it supports + - * / and ()
# for example:
# "1 + 1" returns 2
# (1 + 2) * 3 return 9
# ((1 + 3) / 2) * 5 returns 10

# 1 + 1 => 2
# 1 + 2 * 3 => 7
# [1, +, 2, *, 3]
# [1] => [1, 2] => [1, 6] => 7

import re
from  collections import deque
def calculate(input):
    index = 0
    splitted = input[:]
    q = deque()
    while index < len(splitted):
        cur = splitted[index]
        if cur == '+':
            pass
        elif cur == '-':
            next = splitted[index + 1]
            q.append(-int(next))
            index += 1
        elif cur == '*':
            pre = q.pop()
            next = splitted[index + 1]
            q.append(pre * int(next))
            index += 1
        elif cur == '/':
            pre = q.pop()
            next = splitted[index + 1]
            q.append(pre / int(next))
            index += 1
        else:
            q.append(int(splitted[index]))
        index += 1
    return sum(list(q))

def handle_para(input):
    q = deque()
    index = 0
    while index < len(input):
        cur = input[index]
        if cur == ')':
            inner = []
            pre = q.pop()
            while pre != '(':
                inner.append(pre)
                pre = q.pop()
            q.append(calculate(inner[::-1]))
        else:
            q.append(input[index])
        index += 1
    return q.pop()
print(handle_para(['(', '(', '-', '1', '+', '2', '*', '3', ')', '*', '5', ')']))
