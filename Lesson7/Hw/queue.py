# implement deque by double linked list to achieve O(1) time for pop and push
class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.prev = None

class Deque:
    def __init__(self, size):
        # limit the max queue size
        self._size = size
        self._head = Node(0)
        self._end = Node(0)
        self._head.next = self._end
        self._end.prev = self._head

    def pop():
        pass

    def popLeft():
        pass

    def push(element):
        pass

    def pushLeft(element):
        if self.isFull():
            self.pop()
        pass

    def isFull():
        pass

    def isEmpty():
        pass

q = Deque(3)
print(q.isFull())
q.push(1)
q.push(2)
