# work on tuple
numbers = (1, 2, 3)
one, two, three = numbers
print(one)
print(two)
print(three)

# work on list
numbers = [1, 2, 3]
one, two, three = numbers
print(one)
print(two)
print(three)

# more example
data = ['ACME', 50, 91.1, (2012, 12, 21)]
name, _, _, date = data
print(name)
print(date)

# exception if not matched
name, date = data
