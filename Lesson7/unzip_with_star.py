# # unzip with star
# # record = ('Dave', 'dave@example.com', '773-555-1212', '847-555-1212')
# # name, email, *phone_numbers = record
# # print(name)
# # print(email)
# # print(phone_numbers)
# #
# # # star with star and last
# # name, email, *phone_numbers, last = record
# # print(phone_numbers)
# # print(last)
# #
# # # star will always give you a list
# # name = ["yichi", "chan"]
# # first, *mid, last = name
# # print(first)
# # print(mid)
# # print(last)
# # recursion with star
# my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# def mysum(l):
#     head, *tail = l
#     return head + mysum(tail) if tail else head
# print(mysum(my_list))
#
# # # more than one star => sytax error
# # name = ["yichi", "chan"]
# # first, *mid, *last = name
#
#
#
# 
# input = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# def mysumm(input):
#     first, *other = input
#     return first + mysumm(other) if other else first
# print(mysumm(input))
