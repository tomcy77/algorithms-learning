from collections import defaultdict

d = defaultdict(list)
d['a'].append(1)
d['a'].append(2)
d['b'].append(4)
d['a'].append(2)
print(d)
# def mydic():
#     dic = {}
#     for key, value in enumerate(input):
#         if key not dic:
#             dic[key] = [value]
#         else:
#             dic[key].append(value)

d = defaultdict(set)
d['a'].add(1)
d['a'].add(2)
d['b'].add(4)
d['a'].add(2)
print(d)
