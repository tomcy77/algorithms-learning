# Lesson 7
Data structure

#### Any question from last lesson?
1. QuickSort

## Collections
1. [Python cookbook](https://python3-cookbook.readthedocs.io/zh_CN/latest/index.html)
1. Tuples
  1. unzip variable: v = (a, b, c)
  1. unzip with star
1. keep n element: deque (remove or insert an element from two ends is O(1) vs O(n) in list)
  1. double linked list with pointers to start and end
  1. head <-> n1 <-> n2 <-> end
1. find the largest n element or smallest n element: queue
1. piority queue: revsered queue => key -> -key
   1 -> 2 -> 3 -> 4 => -4 -> -3 -> -2 -> -1
1. multi value dictionary: multidict

## Hw
