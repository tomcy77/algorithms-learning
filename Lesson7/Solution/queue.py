class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.prev = None
    def linkNext(self, next_node):
        self.next = next_node
        next_node.prev = self

class Deque:
    def __init__(self, size):
        # limit the max queue size
        self._size = size
        self._head = Node(None)
        self._end = Node(None)
        self._current_count = 0

    def pop(self):
        if self._current_count == 0:
            print("Error: Current size is 0.")
            return
        end = self._end.prev
        self._end.prev.prev.linkNext(self._end)
        self._current_count -= 1
        return end.data

    def push(self, element):
        new_node = Node(element)
        if self.isEmpty():
            self._head.linkNext(new_node)
            new_node.linkNext(self._end)
            self._current_count += 1
            return

        original_head = self._head.next
        self._head.linkNext(new_node)
        new_node.linkNext(original_head)
        if self.isFull():
            self._end.prev.prev.linkNext(self._end)
            return
        self._current_count += 1
        return

    def isFull(self):
        if self._size == self._current_count:
            return True
        return False

    def isEmpty(self):
        if self._current_count == 0:
            return True
        return False


q = Deque(1)
print(q.isFull())
print(q.isEmpty())
q.push(1)
print(q.pop())
q.push(2)
q.push(1)
print(q.pop())
print(q.pop())
