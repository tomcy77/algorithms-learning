from collections import Counter

class Solution:
    def canArrange(self, arr: List[int], k: int) -> bool:
        mod = Counter()
        for num in arr:
            mod[num % k] += 1
        for key in mod.keys():
            if key == 0:
                if mod[key] % 2 == 0:
                    continue
                return False
            if mod[key] != mod[k-key]:
                return False
        return True
