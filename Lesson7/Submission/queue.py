# implement deque by double linked list to achieve O(1) time for pop and push
# rule: FIFO (First in First out)
class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.prev = None

class Deque:
    def __init__(self, size):
        # limit the max queue size
        self._size = size
        self._head = Node(None)
        self._end = Node(None)

    def pop(self):
        pass

    def push(self, element):
        if self.isEmpty():
            self._head = Node(element)
        if self.isFull():
            self._end = Node(self._end.prev)
            new_head = Node(element)
            new_head.next = self._head
            self._head.prev = new_head
            self._head = new_head
        else:
            while self._head.next
                self._head = self._head.next
            self._head.next = Node(element)
    def isFull(self):
        if self._head:
            counter = 1
            while self._head.next:
                counter += 1
        else:
            return False
        if counter == self._size:
            return True
        else:
            return False

    def isEmpty(self):
        if not self._head.data:
            return True
        else:
            return False

q = Deque(3)
print(q.isFull())
print(q.isEmpty())
q.push(1)
q.push(2)
print(q._head.data)
#print(q._head.next.data)
