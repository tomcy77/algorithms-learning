# 200. Number of Islands
# Given a 2d grid map of '1's (land) and '0's (water), count the number of islands.
#  An island is surrounded by water and is formed by connecting adjacent lands
#  horizontally or vertically. You may assume all four edges of the grid are all
#   surrounded by water.
# Example 1:
#
# Input: grid = [
#   ["1","1","1","1","0"],
#   ["1","1","0","1","0"],
#   ["1","1","0","0","0"],
#   ["0","0","0","0","0"]
# ]
# Output: 1
# Example 2:
#
# Input: grid = [
#   ["1","1","0","0","0"],
#   ["1","1","0","0","0"],
#   ["0","0","1","0","0"],
#   ["0","0","0","1","1"]
# ]
# Output: 3
class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        island_counter = 0
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == '1':
                    island_counter += 1
                    self.sweep(grid, i, j)
        return island_counter
    def sweep(self, grid, k, l):
        if k < 0 or k >= len(grid) or l < 0 or l >= len(grid[0]):
            return
        if grid[k][l] == '0':
            return
        grid[k][l] = '0'
        self.sweep(grid, k-1, l)
        self.sweep(grid, k+1, l)
        self.sweep(grid, k, l-1)
        self.sweep(grid, k, l+1)

# leetcode accepted
