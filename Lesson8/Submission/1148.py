# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
    def goodNodes(self, root: TreeNode) -> int:
        count = [1]
        self.traverse(root, root.val, count)
        return count[0]

    def traverse(self, root: TreeNode, biggest: int, count: list):
        if root.left:
            if root.left.val >= biggest:
                count[0] += 1
            self.traverse(root.left, max(biggest, root.left.val), count)
        if root.right:
            if root.right.val >= biggest:
                count[0] += 1
            self.traverse(root.right, max(biggest, root.right.val), count)

root = TreeNode(2)
root.right = TreeNode(4)
root.right.left = TreeNode(10)
root.right.right = TreeNode(8)
print(Solution().goodNodes(root))
