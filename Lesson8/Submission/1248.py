# Given an array of integers nums and an integer k. A subarray is called nice
# if there are k odd numbers on it.
#
# Return the number of nice sub-arrays.
#
# Example 1:
# Input: nums = [1,1,2,1,1], k = 3
11211
1121 -> count += 1
121
1211 -> count += 1
# Output: 2
# Explanation: The only sub-arrays with 3 odd numbers are [1,1,2,1] and [1,2,1,1].
# Example 2:
# Input: nums = [2,4,6], k = 1
# Output: 0
# Explanation: There is no odd numbers in the array.
# Example 3:
# Input: nums = [2,2,2,1,2,2,1,2,2,2], k = 2
# Output: 16
#
# Constraints:
#
# 1 <= nums.length <= 50000
# 1 <= nums[i] <= 10^5
# 1 <= k <= nums.length
class Solution:
    def numberOfSubarrays(self, nums: List[int], k: int) -> int:
        left = 0
        odd_counter = 0
        left_counter = 0
        result = 0
        for right, element in enumerate(nums):
            if element% 2 == 1:
                odd_counter += 1
            elif element% 2 == 0:
                result += left_counter
            if odd_counter == k:
                left_counter = 0
            while odd_counter == k:
                if nums[left]% 2 == 1:
                    odd_counter -= 1
                left_counter += 1
                result += 1
                left += 1
        return result

#leetcode accepted
