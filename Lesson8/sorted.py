# return sorted array
# input = [5, 2, 3, 1, 4]
# sortedList = sorted(input)
# print(input)
# print(sortedList)
# no return, sort in place, slightly faster
# input = [5, 2, 3, 1, 4]
# print(input.sort())
# print(input)
#
# sorted can accept any iterable.
# list.sort() only define in list
# print(sorted({1: 'D', 2: 'B', 3: 'B', 4: 'E', 5: 'A'}))

# pass key if sort specially
student_tuples = [
    ('john', 'A', 15),
    ('jane', 'B', 12),
    ('dave', 'B', 10),
]

print(sorted(student_tuples, key=lambda student: student[2]))   # sort by age

# # work with class also
# class Student:
#     def __init__(self, name, grade, age):
#         self.name = name
#         self.grade = grade
#         self.age = age
#     def __repr__(self):
#         return repr((self.name, self.grade, self.age))
# student_objects = [
#     Student('john', 'A', 15),
#     Student('jane', 'B', 12),
#     Student('dave', 'B', 10),
# ]
# print(sorted(student_objects, key=lambda student: student.age))   # sort by age

# # Ascending and Descending (reverse = True)
# print(sorted(student_tuples, key=itemgetter(2), reverse=True))
