from collections import Counter

cnt = Counter()
input = ['red', 'blue', 'red', 'green', 'blue', 'blue']
for word in input:
    cnt[word] += 1
print(cnt)

cnt2 = {}
input = ['red', 'blue', 'red', 'green', 'blue', 'blue']
for word in input:
    if word not in cnt2:
        cnt2[word] = 1
    else:
        cnt2[word] += 1
print(cnt2)
