class Solution:
    def numRollsToTarget(self, d: int, f: int, target: int) -> int:
        memory = [[0 for _ in range(d+1)] for _ in range(target+1)]
        for i in range(f+1):
            memory[i][1] = 1

        for i in range(1, f+1):
            if target - i < 0 :
                return ways
            ways += self.numRollsToTarget(d-1, f, target-i)
        return ways
