import math
from collections import Counter

class Solution:
    def numRollsToTarget(self, d: int, f: int, target: int) -> int:
        result = []
        self.combination([], d, f, target, result)
        sum = 0
        for com in result:
            sum += self.count(com) % (10^9 + 7)
        return int(sum  % (10^9 + 7))

    def count(self, input):
        c = Counter(input)
        divider = 1
        for k in c:
            divider *= math.factorial(c[k])
        return math.factorial(len(input)) / divider

    def combination(self, cur, d, f, target, result):
        if d == 0:
            if target == 0:
                result.append(cur)
            return
        for i in range(1, f+1):
            if cur and cur[-1] > i:
                continue
            self.combination(cur + [i], d - 1, f, target - i, result)
        return

print(Solution().numRollsToTarget(30, 30, 500))
