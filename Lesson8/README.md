# Lesson 8

#### Any question from last lesson?
1. Unzip
1. Queue
1. Double linked list
1. defaultdict

## Data structure
1. Last question: [queue vs deque vs heap](https://prismoskills.appspot.com/lessons/Algorithms/Chapter_03_-_Queue_Dequeue_and_Priority_Queue.jsp)
  1. Queue is a list where insertion is done at one end and removal is done at the other end.
  1. Dequeue is a list where every end supports insertion and removal. [Link](https://docs.python.org/2/library/collections.html#collections.deque)
  1. Priority queue does not have any ends.
In a priority queue, elements can be inserted in any order but removal of the elements is in a sorted order.
  1. Heap is the data structure best suited to implement Priority Queues. Heaps are represented using arrays(usually) and can get maximum(or highest priority) element in O(1). The Heaps are visualized as a binary tree, with elements stored internally in an array
1. collections: [Counter and OrderedDict](https://docs.python.org/2/library/collections.html#module-collections)
  1. Counter: dictionary with counter
  1. OrderedDict: dictionary that will remember the insertion order
  1. Sorted: sorted.py
1. zip: pair two list one by one.
1. sort in dictionary: use zip to reverse the key and value
1. dictionary as a set: dict.keys() and dict.items() returns a set but dict.values() doesn't
  1. set can support & - | operation

## Hw
