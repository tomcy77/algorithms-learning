# Lesson 25
Testing, Debugging and Exceptions

## Testing
1. why testing?
1. how to test?
  1. [unittest library](https://docs.python.org/3/library/unittest.html)
  1. real example
  1. mocking
1. testing the print statement
  1. [unittest.mock.patch](https://docs.python.org/3/library/unittest.mock.html)
  1. the order of decorator is reversed in the input parameters
1. patch objects in unit tests
  1. [MagicMock](https://docs.python.org/3/library/unittest.mock.html#unittest.mock.MagicMock)
1. Testing exceptions
  1. assertRaises
  1. [assertRaisesRegex](https://docs.python.org/3/library/unittest.html#unittest.TestCase.assertRaisesRegex)
## Hw
