# Definition for a binary tree node.
from typing import List

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
    def __repr__(self):
        print("Left = {}, Right = {}".format(self.left, self.right))
        return "Node()"

class Solution:
    def allPossibleFBT(self, N: int) -> List[TreeNode]:
        result = []
        if N == 1:
            return [TreeNode()]
        if N % 2 == 0:
            return result
        for i in range(N):
            # 0 -> N-2
            left = self.allPossibleFBT(i)
            right = self.allPossibleFBT(N-i-1)
            if not left or not right:
                continue
            for l in left:
                for r in right:
                    head = TreeNode()
                    head.left = l
                    head.right = r
                    result.append(head)
        return result

s = Solution()
result = s.allPossibleFBT(5)
print(result[0].right.right)
print(result[0].right.left)
print(result[0].left)
print(result[0].right)
  #  o
  # o  o
  #   0  0
