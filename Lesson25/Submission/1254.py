from typing import List
class Solution:
    def closedIsland(self, grid: List[List[int]]) -> int:
        result = 0
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == 0:
                    result += self.recursion(grid, i, j)
        return result

    def recursion(self,grid, i, j) -> bool:
        if i >= len(grid) or j >= len(grid[0]) or i < 0 or j  < 0:
            return False
        if grid[i][j] == 1:
            return True
        grid[i][j] = 1
        b1 = self.recursion(grid ,i+1 ,j)
        b2 = self.recursion(grid ,i ,j+1)
        b3 = self.recursion(grid ,i-1 ,j)
        b4 = self.recursion(grid ,i ,j-1)
        return b1 & b2 & b3 & b4

#accepted
