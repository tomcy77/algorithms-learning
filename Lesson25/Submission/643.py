# V1
from typing import List

class Solution:
    def findMaxAverage(self, nums: List[int], k: int) -> float:
        result = 0
        output = sum(nums[:k])
        for i in range(1, len(nums)):
            if k + i > len(nums):
                return output / k
            result = sum(nums[i:(k+i)])
            if result > output:
                output = result
        return output / k

# Time Limited Exceeded


#V2

class Solution:
    def findMaxAverage(self, nums: List[int], k: int) -> float:
        total = maximum = sum(nums[:k])
        for i in range(1, len(nums)-k+1):
            total += nums[i+k-1]
            total -= nums[i-1]
            maximum = max(maximum, total)
        return maximum/k

#accepted
