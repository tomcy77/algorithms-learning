from unittest.mock import MagicMock

m = MagicMock(return_value = 10)
print(m(1, 2, debug=True)) # 10

m.assert_called_with(1, 2, debug=True)
# m.assert_called_with(1, 2) # assert failed

m.upper.return_value = 'HELLO'
print(m.upper('hello'))
print(m.upper('123'))

# assert it's called
assert m.upper.called
m.split.return_value = ['hello', 'world']
print(m.split('hello world'))

m.split.assert_called_with('hello world')

print(m['blah']) # return a MagicMock
assert m.__getitem__.called
m.__getitem__.assert_called_with('blah')


# another example
import unittest
from unittest.mock import patch
import io
import mymodule

sample_data = io.BytesIO(b'''\
"IBM",91.1\r
"AA",13.25\r
"MSFT",27.72\r
\r
''')

class Tests(unittest.TestCase):
    def setUp(self):
        ...

    def tearDown(self):
        ...

    @patch('mymodule.urlopen', return_value=sample_data)
    def test_dowprices(self, mock_urlopen):
        p = mymodule.dowprices()
        self.assertTrue(mock_urlopen.called)
        self.assertEqual(p,
                         {'IBM': 91.1,
                          'AA': 13.25,
                          'MSFT' : 27.72})

if __name__ == '__main__':
    unittest.main()
