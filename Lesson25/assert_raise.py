import unittest
import errno

# A simple function to illustrate
def parse_int(s: str):
    return int(s)

class TestConversion(unittest.TestCase):
    def test_good_int(self):
        self.assertEqual(parse_int('123'), 123)

    def test_bad_int(self):
        self.assertRaises(ValueError, parse_int, 'N/A')

    # test the real error content
    # https://docs.python.org/3/library/errno.html
    def test_file_not_found(self):
        try:
            f = open('/file/not/found')
        except IOError as e:
            self.assertEqual(e.errno, errno.ENOENT)
        else:
            self.fail('IOError not raised')

    # bad example
    def test_bad_int_manually(self):
        try:
            r = parse_int('N/A')
        except ValueError as e:
            self.assertEqual(type(e), ValueError)

    # correct one
    def test_bad_int_manually_correct(self):
        try:
            r = parse_int('N/A')
        except ValueError as e:
            self.assertEqual(type(e), ValueError)
        else:
            self.fail('ValueError not raised')

    def test_bad_int(self):
        self.assertRaisesRegex(ValueError, 'invalid literal .*',
                                       parse_int, 'N/A')

if __name__ == '__main__':
    unittest.main()
