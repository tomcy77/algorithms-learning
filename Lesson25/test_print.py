from io import StringIO
import unittest
from unittest import TestCase
from unittest.mock import patch
import mymodule

class TestURLPrint(TestCase):
    def test_url_gets_to_stdout(self):
        protocol = 'http'
        host = 'www'
        domain = 'example.com'
        expected_url = '{}://{}.{}\n'.format(protocol, host, domain)

        # StringIO: https://docs.python.org/3/library/io.html#io.StringIO
        with patch('sys.stdout', new=StringIO()) as fake_out:
            mymodule.urlprint(protocol, host, domain)
            self.assertEqual(fake_out.getvalue(), expected_url)

    # Second way: Decorator
    @unittest.mock.patch('sys.stdout', new_callable=StringIO)
    def test_url_gets_to_stdout(self, fake_out):
        protocol = 'http'
        host = 'www'
        domain = 'example.com'
        expected_url = '{}://{}.{}\n'.format(protocol, host, domain)

        mymodule.urlprint(protocol, host, domain)

        self.assertEqual(fake_out.getvalue(), expected_url)

    # third way: Setup
    def setUp(self):
        self.patcher1 = patch('sys.stdout')
            
    def test_use(self):
        self.patcher1.return_value = "123"

if __name__ == '__main__':
    unittest.main()
