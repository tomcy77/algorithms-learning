# input = [9, 4, 5, 1, 10, 6]
# Given a unsorted array as above, the selection sort works as follow:
# 1. find the smallest elemet in the input
# 2. insert it into output
# 3. remove it from input
# 4. loop until the input is empty


input = [9, 4, 5, 1, 10, 6]

def SelectionSort(input):
    sorted = []
    while input:
        index = input.index(min(input))
        sorted.append(input[index])
        input.remove(input[index])
    return sorted

print(SelectionSort(input))
