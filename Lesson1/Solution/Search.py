# input = [1, 2, 3, 4, 6, 8, 9, 11]
# target = 5 return None
# target = 1 return 0 (position)

input = [1, 2, 3, 4, 6, 8, 9, 11]

def Search(input, target):
    for i in range(len(input)):
        if input[i] == target:
            return i
    return None

print(Search(input, 3))
