# input = [1, 2, 4, 6, 8, 9, 10, 20]
# target = 19 return [5, 6]
# target = 15 return []
# target = 20 not return [6, 6] XXXXXX

input = [1, 2, 4, 6, 8, 9, 10, 20]

def TwoSum(input, target):
    for i in range(len(input)):
        for j in range(i+1, len(input)):
            if input[i] + input[j] == target:
                return [i, j]
    return []

print(TwoSum(input, 30))
