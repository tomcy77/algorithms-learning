input = [9, 4, 5, 1, 10, 6]

# def sort(input):
#     output = []
#     for element in input:
#         print(output)
#         if len(output) == 0:
#             output.append(element)
#             continue
#         for i in range(len(output)):
#             if i == len(output) - 1 and output[i] < element:
#                 output.append(element)
#                 break
#             if i == 0 and element < output[0]:
#                 output.insert(0, element)
#                 break
            # if output[i] < element < output[i+1]:
            #     output.insert(i+1, element)
            #     break
#
#     return output
#
# print(sort(input))

def sort(input):
    output = []
    for element in input:
        if(len(output) == 0):
            output.append(element)
        else:
            for i in range(len(output)):
                cur = output[i]
                if(element < cur):
                    output.insert(i, element)
                    break
                elif(element > cur and i == len(output)-1):
                    output.append(element)
                    break
                else:
                    continue
    return output

input2 = [1]
print(sort(input2))
