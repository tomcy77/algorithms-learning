class DayNames:
    def __init__(self, dataval=None):
        self.dataval = dataval
        self.nextval = None

e1 = DayNames('Mon')
e2 = DayNames('Tue')
e3 = DayNames('Wed')

e1.nextval = e2
e2.nextval = e3

print(e1.nextval)
print(e1.nextval.dataval)
