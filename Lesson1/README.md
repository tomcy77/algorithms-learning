# Lesson 1

## Goals
Make sure all required libraries are installed and pull repo from Bitbucket seccussfully.

### Prerequisite
1. install [Windows Python](https://www.python.org/ftp/python/3.8.3/python-3.8.3.exe) (3.8)
1. install [Git](https://git-scm.com/downloads)
1. install [Atom](https://atom.io/) (teletype)
1. create an account in [Bitbucket](https://bitbucket.org/product)

### Class
1. Git usage
  1. pull repo from Bitbucket.
  ```
  git clone https://tomcy77@bitbucket.org/tomcy77/algorithms-learning.git
  ```
  1. create a branch, make some changes and create a merge request.
  ```
  git commit
  git push
  ```
1. Book: [Introduction to Algorithms](https://web.ist.utl.pt/~fabio.ferreira/material/asa/clrs.pdf)
1. Tools:
  1. IDE vs Editor vs Compiler
  1. [Pycharm](https://www.jetbrains.com/pycharm/) (Python IDE, recommended)
  1. Atom (Editor with some plugins)
  1. Recommended workflow: PyCharm and Git
1. A simple example: HelloWorld.py
1. [Convention](https://www.python.org/dev/peps/pep-0008/):
  1. [Indention: Use 4 spaces per indentation level](https://www.python.org/dev/peps/pep-0008/#indentation)
  1. [Use Space instead of Tab](https://www.python.org/dev/peps/pep-0008/#tabs-or-spaces)
  1. [Imports](https://www.python.org/dev/peps/pep-0008/#imports) (Imports.py)
  1. [Class name and varieble name](https://www.python.org/dev/peps/pep-0008/#class-names)
1. What is Algorithm?
 1. Computation steps, inputs and outputs.
 1. Example: sorting may be the most important and well defined problem for computer science (Sorting.py)
 1. Example: Google search (relevent information, sorting and ads)
 1. Example: Facebook (face recognition, friend recomendation and ads)
 1. Algorithms: Quick sort, Merge sort, Greedy algorithm, recursion and dynamic programming.
 1. How to evaluate a algorithm? Complexity -> NP-hard problems
1. Insertion sort
 1. an empty array
 1. start from the first element and insert it into the correct position
 1. sorted after we finish each element
 1. InsertionSort.py
1. Homework
 1.
