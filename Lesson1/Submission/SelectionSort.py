# input = [9, 4, 5, 1, 10, 6]
# Given a unsorted array as above, the selection sort works as follow:
# 1. find the smallest elemet in the input
# 2. insert it into output
# 3. remove it from input
# 4. loop until the input is empty


input = [9, 4, 5, 1, 10, 6]
output = [ ]

def SelectionSort(input) :
    while len(input) != 0 :
        num = 0
        for i in range(1, len(input)) :
            if input[num] >= input[i] :
                num = i

        output.append(input[num])
        input.remove(input[num])

    return output

print(SelectionSort(input))

def SelectionSort2(input):
    sorted = []
    while input:
        index = input.index(min(input))
        sorted.append(input[index])
        input.remove(index)
    return sorted

print(SelectionSort2(input))
