# input = [1, 2, 4, 6, 8, 9, 10, 20]
# target = 19 return [5, 6]
# target = 15 return []
# target = 20 not return [6, 6] XXXXXX

input = [1, 2, 4, 6, 8, 9, 10, 20]

def TwoSum(input, target):
    i = 0
    j = i + 1
    while i != len(input)-1 and j != len(input) :
        if target - input[i] - input[j] == 0 :
            return[i,j]
            break
        elif j == len(input)-1 :
            i = i + 1
            j = i + 1
        else:
            j = j + 1
    return [ ]

print(TwoSum())

# recommendation
def TwoSum2(input, target):
    for i in range(len(input)):
        for j in range(i+1, len(input)):
            if input[i] + input[j] == target:
                return [i, j]
    return []
