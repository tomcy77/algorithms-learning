# input = [1, 2, 3, 4, 6, 8, 9, 11]
# target = 5 return None
# target = 1 return 0 (position)

input = [1, 2, 3, 4, 6, 8, 9, 11]

def Search(input, target):
    # when there is nothing in array
    if len(input) == 0:
        return None
    elif target in input: # when the target is in array
        i = 0
        while i < len(input) : # use while loop because don't know if there are several targets in array or not
            if target - input[i] == 0 :
                return i
                i = i + 1
            else:
                i = i + 1

# when the target can't be find in the array
    else:
        return None

## recommendation

input = [1, 2, 3, 4, 6, 8, 9, 11]

def Search2(input, target):
    for i in range(len(input)):
        if input[i] == target:
            return i
    return None

print(Search())
