#  # Python lists
# x = [1, 2, 3, 4]
# y = [5, 6, 7, 8]
#
# print(x * 2)
# # [1, 2, 3, 4, 1, 2, 3, 4]
#
# # print(x + 10)
# # Traceback (most recent call last):
# #     File "<stdin>", line 1, in <module>
# # TypeError: can only concatenate list (not "int") to list
#
# print(x + y)
# # [1, 2, 3, 4, 5, 6, 7, 8]

# Numpy arrays
import numpy as np

ax = np.array([1, 2, 3, 4])
ay = np.array([5, 6, 7, 8])
print(ax + ay)
print(ax * 2) # apply to each element
# array([2, 4, 6, 8])
print(ax + 10)
# array([11, 12, 13, 14])
print(ax + ay)
# array([ 6, 8, 10, 12])
print(ax * ay) # dot
# array([ 5, 12, 21, 32])

# apply fuction
def f(x):
    return 3*x**2 - 2*x + 7
print(f(ax))
