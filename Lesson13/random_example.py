import random

# values = [1, 2, 3, 4, 5, 6]
# # get one
# print(random.choices(values, k=10))
# print(random.choice(values))
#
# # get multiple
# print(random.sample(values, 3))
#
# # shuffle
# random.shuffle(values)
# print(values)
#
# # generate random int
# print(random.randint(0, 10))
# print(random.randint(0, 100))
#
# # generate float from 0 to 1
# print(random.random())

# set random seed
random.seed(123)
print(random.randint(0, 10))
print(random.randint(0, 10))
