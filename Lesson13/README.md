# Lesson 13
Numpy and Date

#### Any question from last lesson?
String and date

## Numpy
1. [numpy](https://numpy.org/): scientific computing with Python
1. python list vs numpy.array
1. numpy has general functions like sqrt and cos which can replace the one in Python default library.
1. numpy 2d array: similar to pandas
1. [np.where](https://numpy.org/doc/stable/reference/generated/numpy.where.html): return elements chosen from x or y depending on condition.
1. [matrix](https://numpy.org/doc/stable/reference/generated/numpy.matrix.html): transpose, inverse, multiply, determinant, eigenvalues
1. [random](https://docs.python.org/3/library/random.html): choice, sample, shuffle, randint, random
1. [python.random vs numpy.random](https://stackoverflow.com/questions/7029993/differences-between-numpy-random-and-random-random-in-python)
1. [datetime](https://docs.python.org/2/library/datetime.html): date manipulation like add, minus

## Hw
