# Given a board with m by n cells, each cell has an initial state live (1) or dead (0).
#  Each cell interacts with its eight neighbors (horizontal, vertical, diagonal)
#  using the following four rules (taken from the above Wikipedia article):
#
# Any live cell with fewer than two live neighbors dies, as if caused by under-population.
# Any live cell with two or three live neighbors lives on to the next generation.
# Any live cell with more than three live neighbors dies, as if by over-population..
# Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
# Write a function to compute the next state (after one update) of the board given its current state.
#
# The next state is created by applying the above rules simultaneously to every cell in the current state,
# where births and deaths occur simultaneously.
#
# Example:
#
# Input:
# [
#   [0,1,0],
#   [0,0,1],
#   [1,1,1],
#   [0,0,0]
# ]
# Output:
# [
#   [0,0,0],
#   [1,0,1],
#   [0,1,1],
#   [0,1,0]
# ]
# Follow up:
#
# Could you solve it in-place? Remember that the board needs to be updated at the same time:
# You cannot update some cells first and then use their updated values to update other cells.
# In this question, we represent the board using a 2D array. In principle, the board is infinite,
#  which would cause problems when the active area encroaches the border of the array.
#   How would you address these problems?
import copy

class Solution:
    def gameOfLife(self, board) -> None:
        for i in range(len(board)):
            for j in range(len(board[i])):
                counter = self.LivingNeighborCounter(board, i, j)
                if board[i][j] == 1 and (counter < 2 or counter > 3):
                    board[i][j] = 'x'
                if board[i][j] == 0 and counter == 3:
                    board[i][j] = 'o'
        for i in range(len(board)):
            for j in range(len(board[i])):
                if board[i][j] == 'x':
                    board[i][j] = 0
                if board[i][j] == 'o':
                    board[i][j] = 1
        return board[:][:]

    def LivingNeighborCounter(self, board, i, j):
        l = [[1, 0], [1, 1], [1, -1], [0, 1], [0, -1], [-1, -1], [-1, 0], [-1, 1]]
        counter = 0
        for direction in l:
            counter += self.get(board, i + direction[0], j + direction[1])
        return counter

    def get(self, board, i, j):
        if i < 0 or i >= len(board) or j < 0 or j >= len(board[0]):
            return 0
        if board[i][j] == 1 or board[i][j] == 'x':
            return 1
        return 0

    def finalize(self, board) -> None:
        prev = board[:][:]
        next = self.gameOfLife(board)
        max_depth = 1000
        counter = 0
        while prev != next and counter < max_depth:
            prev = copy.deepcopy(next)
            next = self.gameOfLife(next)
            counter += 1
        if counter == max_depth:
            print("not stable")
        else:
            print("stable after %d times run" % counter)
            print(prev)
#leetcode accepted

table = [
  [0,1,0],
  [0,0,1],
  [1,1,1],
  [0,0,0]
]
# Solution().finalize(table)
# print(table[:][:])

stable = [
  [0,0,0],
  [0,0,0],
  [0,1,1],
  [0,1,1]
]
print(Solution().gameOfLife(stable))
