from datetime import timedelta

a = timedelta(days=2, hours=6)
b = timedelta(hours=4.5)
c = a + b
print(c.days)
# 2
print(c.seconds)
# 37800
print(c.seconds / 3600)
# 10.5
print(c.total_seconds() / 3600)
# 58.5

from datetime import datetime
a = datetime(2020, 9, 26)
print(a + timedelta(days=10))
# 2012-10-06 00:00:00
b = datetime(2020, 12, 18)
d = b - a
print(d.days)
# 89
now = datetime.today()
print(now)
print(now + timedelta(minutes=10))
