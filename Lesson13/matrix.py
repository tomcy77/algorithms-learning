import numpy as np

m = np.matrix([[1,-2,3],[0,4,5],[7,8,-9]])
print(m)
# matrix([[ 1, -2, 3],
#         [ 0, 4, 5],
#         [ 7, 8, -9]])
#
# # Return transpose
# print(m.T)
# # matrix([[ 1, 0, 7],
# #         [-2, 4, 8],
# #         [ 3, 5, -9]])
#
# # Return inverse
# print(m.I)
# # matrix([[ 0.33043478, -0.02608696, 0.09565217],
# #         [-0.15217391, 0.13043478, 0.02173913],
# #         [ 0.12173913, 0.09565217, -0.0173913 ]])
#
#
# # Create a vector and multiply
# v = np.matrix([[2],[3],[4]])
# print(v)
# # matrix([[2],
# #         [3],
# #         [4]])
# print(m * v)
# # matrix([[ 8],
# #         [32],
# #         [ 2]])
