import numpy as np

a = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
print(a)

# row 0
print(a[0])

# column 0
print(a[:, 0])

# sub array
print(a[1:3, 1:3])

# addition
a[1:3, 1:3] += 10
print(a)

# Broadcast a row vector across an operation on all rows
b = a + [100, 101, 102, 103]
print(b)

# Conditional return on an array
print(np.where(a < 10, a, 10))
