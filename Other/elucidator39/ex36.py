from sys import exit
door = ['left','right']
entering = "You get in to the room and the door behind you just disapeared."
hurt = False

def goal_room():
    print(entering)
    print("There is a big glory ball floating in the room.")
    print("'Welcome to limbo you brave warrior.' The ball says.")
    print("The ball asks you with a gorgeous voice,\n'Which animal do you like more,dog or cat?'")
    choice = input()
    if choice == 'dog':
        print("Your soul is polluted and shall not enter heaven")
        print("Go to hell!")
        exit(0)
    elif choice == 'cat':
        print("'You have a pure heart and glorius soul.'")
        print("'The door of heaven is opened for you.'")
        print("Go to heaven!")
        exit(0)
    else:
        print("'Interesting, you choose another animal, huh.'")
        print("'Tell me why you like it then.'")
        reason = input('> ')
        if len(reason) >= 25:
            print("'Well, I can agree with you somehow.'")
            print("'Even you don't like cat, you can enter the door of heaven though.'")
            print("Go to heaven!")
            exit(0)
        elif len(reason) < 25:
            print("'Obviously, you are a lying basterd.'")
            print("'People who like you shall never be saved.'")
            print("Go to hell!")
            exit(0)

def teleport_room():
    print(entering)
    print("The room start shining and you cannot see any thing.")
    start()

def lion_room():
    global hurt
    print(entering)
    print("There is a adult lion staring at you in the middle of the room.")
    print("And there is a cat teaser wand next to you.")
    print("What do you do?")
    choice = input("A.Use the tease wand to play with the lion.\nB.Fight the lion.\n> ")
    if choice == 'A':
        print("The lion play with you for a while and looks tired.")
        print("It seems safe to pass him now.")
        print("There is only one door in the room, you have no choice this time.")
        goal_room()
    elif choice == 'B':
        print("You try to fight the lion and somehow defeat him, and get injured.")
        if hurt == True:
            print("You got serious injured and died")
            exit(0)
        else:
            print("There is only one door in the room, you have no choice this time.")
            goal_room()
    else:
        print("Dude, pick an action or you will be stuck here forever.")
    lion_room()

def trap_room():
    global hurt
    print(entering)
    print("There are 9 blocks on the roof")
    print("Choose one block and you can step on it to get pass this room.")
    choice = input("Pick a number between 1 to 9.\n>")
    if choice == '1' or choice == '3' or choice == '9':
        print("The floor looks strong enough to carry you.")
        print("Now you're acrossed the floor, choose next door in these two.")
        while True :
            next = input("A. Get into the %s door.\nB. get in to the %s door.\n> " %(door[0],door[1]))
            if next == 'A':
                teleport_room()
            elif next == 'B':
                lion_room()
            else :
                print("Dude, pick a door or you will be stuck here forever.")
    elif choice == '2' or choice == '4' or choice == '8':
        print("The floor seems to be too weak to carry you.")
        print("You fall into the hole and get hurt.")
        if hurt == True:
            print("You got serious injured and died")
            exit(0)
        else:
            hurt = True
    elif choice == '5' or choice == '6' or choice == '7':
        print("The floor seems to be too weak to carry you.")
        print("You fall in to the hole and there were so many hungry beasts.")
        print("You were scattered in pieces.")
        print("You died")
        exit(0)
    else:
        print("Dude, pick a number or you will be stuck here forever.")
    trap_room()

def zombie_room():
    global hurt
    print(entering)
    print("There is a zombie in the room, looks like he doesn't notice you.")
    print("What do you do?")
    choice = input("A. Fight the zombie. B. Sneak through the zombie.\n> ")
    if choice == 'A':
        print("You sucessfully killed the zombie by hand, but get hurted.")
        if hurt == True:
            print("You got serious injured and died.")
            exit(0)
        else:
            hurt = True
            print("Now choose a door you want to pass.")
            while True :
                next = input("A. Get into the %s door.\nB. get in to the %s door.\n> " %(door[0],door[1]))
                if next == 'A':
                    teleport_room()
                elif next == 'B':
                    lion_room()
                else :
                    print("Dude, pick a door or you will be stuck here with a zombie forever.")
    elif choice == 'B':
        print("You sucessfully sneak off the zombie, now choose a door you wan to pass.")
        while True :
            next = input("A. Get into the %s door.\nB. get in to the %s door.\n> " %(door[0],door[1]))
            if next == 'A':
                teleport_room()
            elif next == 'B':
                lion_room()
            else :
                print("Dude, pick a door or you will be stuck here with a zombie forever.")
    else:
        print("Dude, pick an action or you will be stuck here with a zombie forever.")
        zombie_room()

def start():
    while True:
        print("You are now in a dark room and there are two doors in front of you.")
        print("What do you do?")
        next = input("A. Get into the %s door.\nB. get in to the %s door.\n> " %(door[0],door[1]))
        if next == 'A':
            trap_room()
        elif next == 'B':
            zombie_room()
        else :
            print("Dude, pick a door or you will be stuck here forever.")


start()
