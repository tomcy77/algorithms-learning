import csv

# with open('stock.csv') as f:
#     f_csv = csv.reader(f)
#     headers = next(f_csv)
#     print(headers)
#     for row in f_csv:
#         # Process row
#         print(row)

# use Pandas
import pandas as pd

data = pd.read_csv('stock.csv')
print(data)
# print(type(data.iloc[0, 1]))
#
# specify different delimiter
# Example of reading tab-separated values
# with open('stock.tsv') as f:
#     f_tsv = csv.reader(f, delimiter='\t')
#     for row in f_tsv:
#         # Process row
#         ...

# Read as string => you need to convert it manually
# col_types = [str, float, str, str, float, int]
# with open('stock.csv') as f:
#     f_csv = csv.reader(f)
#     headers = next(f_csv)
#     for row in f_csv:
#         # Apply conversions to the row items
#         row = tuple(convert(value) for convert, value in zip(col_types, row))
#         print(row)
