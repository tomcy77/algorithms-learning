import os

path = '/Users/beazley/Data/data.csv'

# Get the last component of the path
print(os.path.basename(path))
# 'data.csv'

# Get the directory name
print(os.path.dirname(path))
# '/Users/beazley/Data'

# Join path components together
print(os.path.join('tmp', 'data', os.path.basename(path)))
# 'tmp/data/data.csv'

# Expand the user's home directory
path = '~/Data/data.csv'
print(os.path.expanduser(path))
# '/Users/beazley/Data/data.csv'

# Split the file extension
print(os.path.splitext(path))
