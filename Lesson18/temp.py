from tempfile import TemporaryFile

with TemporaryFile('w+t') as f:
    # Read/write to the file
    f.write('Hello World\n')
    f.write('Testing\n')

    # Seek back to beginning and read the data
    f.seek(0)
    data = f.read()
    print(data)

# Temporary file is destroyed
#
# or manually close
f = TemporaryFile('w+t')
# Use the temporary file
# ...
f.close()
# File is destroyed
#
# If you want a file name
from tempfile import NamedTemporaryFile

with NamedTemporaryFile('w+t') as f:
    print('filename is:', f.name)
    #...

# File automatically destroyed

# create a temp directory
from tempfile import TemporaryDirectory

with TemporaryDirectory() as dirname:
    print('dirname is:', dirname)
    # Use the directory
    # ...
# Directory and all contents destroyed
