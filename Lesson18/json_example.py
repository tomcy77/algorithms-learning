import json

data = {
    'name' : 'ACME',
    'shares' : 100,
    'price' : 542.23
}

json_str = json.dumps(data)
print(json_str)

data = json.loads(json_str)
print(data)
print(type(data))
#
# False -> false
# True -> true
# None -> null
print(json.dumps(False))
d = {'a': True,
     'b': 'Hello',
     'c': None}
print(json.dumps(d))
#
# class serialization
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
p = Point(2, 3)

# json.dumps(p)
# give you error

# define serialization function
def serialize_instance(obj):
    d = { '__classname__' : type(obj).__name__ }
    d.update(vars(obj))
    return d

classes = {
    'Point' : Point
}

def unserialize_object(d):
    clsname = d.pop('__classname__', None)
    if clsname:
        cls = classes[clsname]
        obj = cls.__new__(cls) # Make instance without calling __init__
        for key, value in d.items():
            setattr(obj, key, value)
        return obj
    else:
        return d
p = Point(2,3)
s = json.dumps(p, default=serialize_instance)
print(s)
a = json.loads(s, object_hook=unserialize_object)
print(a)
