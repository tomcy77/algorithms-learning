# Lesson 18

#### Any question from last lesson?

## Files and IO
1. path: [os.path](https://docs.python.org/3/library/os.path.html)
  1. use the library instead of string manipulation. System difference such as / and \
  1. file exist: [os.path.exists](https://docs.python.org/3/library/os.path.html#os.path.exists)
  1. file type: isfile, isdir, islink, realpath
  1. file metadata: getsize, getmtime
1. traversal directory: [os.scandir()](https://docs.python.org/3/library/os.html#os.scandir)
1. create temporary file: [tempfile](https://docs.python.org/3/library/tempfile.html)
  1. TemporaryFile: create a temp file
  1. NamedTemporaryFile: create a temp file with name
  1. TemporaryDirectory: create a temp directroy
1. Some parts are skipped

## Data Encoding and Processing
1. how to handle different formats like csv, json ...
1. read and write csv: [csv](https://docs.python.org/3/library/csv.html)
1. read and write json: [json](https://docs.python.org/3/library/json.html)
  1. json format: [example](https://json.org/example.html) like dict
  1. used in many places. ex: POST request
  1. json.dumps and json.load
  1. some minor differences:
    1. True -> true
    1. False -> false
    1. None -> null
  1. json online viewer, extension and so on can help you to structure json

## Hw
