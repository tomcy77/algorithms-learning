from Typing import List

class Solution:
    def solveNQueens(self, n: int) -> List[List[str]]:
       table = [['.' for _ in range(n)] for _ in range(n)]
       result = []
       valid_places = [[1 for _ in range(2*n)] for _ in range(4)]
       self.recur(table, valid_places, n, (0, 0), final_result)
       return final_result

    def recur(self, table: List[List[sstr]], valid_places: List[List[int]],
     left: int, current_place, final_result: List[List[List[str]]]):
    if left == 0:
        final_result.append(table[:])
    x, y = curcurrent_place
    if valid_places[0][x] and valid_places[1][y] and valid_places[2][x + y] and valid_places[3][x-y+n]:
        table[x][y] = 'Q'
        valvalid_places[0][x], valid_places[1][y], valid_places[2][x + y], valid_places[3][x-y+n] = 0, 0, 0, 0
        self.recur(table, valid_places, left-1, (x, y + 1), final_result)
        table[x][y] = '.'
        valvalid_places[0][x], valid_places[1][y], valid_places[2][x + y], valid_places[3][x-y+n] = 1, 1, 1, 1
    else:
        self.recur(table, valid_places, left, (x, y+1), final_result)

N^2
2^(N^2) ->
5 ^ 5 vs 2 ^25
10 ^10 vs 2 ^100
