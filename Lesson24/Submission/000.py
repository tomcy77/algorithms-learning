from typing import List
ELEMENT_SIZE = 3
def valid(mapping: dict, links: List[List[int]]):
    listing = [[], [], []]
    for i in range(len(mapping)):
        if mapping[i] == 'C':
            listing[0].append(i)
        if mapping[i] == 'O':
            listing[1].append(i)
        if mapping[i] == 'H':
            listing[2].append(i)
    linking = []
    for i in range(len(links)):
        for j in range(2):
            linking.append(links[i][j])
    for i in range(ELEMENT_SIZE):
        for j in range(len(listing[i])):
            if i == 0: # when elemnt = 'C'
                if linking.count(listing[i][j]) != 4:
                    return False
            elif i == 1: # when elemnt = 'O'
                if linking.count(listing[i][j]) != 2:
                    return False
            elif i == 2: # when elemnt = 'H'
                if linking.count(listing[i][j]) != 1:
                    return False
    return True

from typing import List
from collections import Counter
def valid2(mapping: dict, links: List[List[int]]):
    # index -> element
    c = Counter()
    for l in links:
        c[l[0]] += 1
        c[l[1]] += 1
    for key in c.keys():
        element = mapping[key]
        if element == 'C' and c[key] != 4:
            return False
        if element == 'O' and c[key] != 2:
            return False
        if element == 'H' and c[key] != 1:
            return False
    return True
mapping = {0: 'C', 1: 'O', 2: 'O'}
links = [[0, 1], [0, 2]]

print(valid2(mapping, links))
