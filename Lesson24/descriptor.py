# Base class. Uses a descriptor to set a value
class Descriptor:
    def __init__(self, name=None, **opts):
        self.name = name
        for key, value in opts.items():
            setattr(self, key, value)

    def __set__(self, instance, value):
        instance.__dict__[self.name] = value
        # print(instance.__dict__)

# Descriptor for enforcing types
class Typed(Descriptor):
    expected_type = type(None)

    def __set__(self, instance, value):
        if not isinstance(value, self.expected_type):
            raise TypeError('expected ' + str(self.expected_type))
        super().__set__(instance, value)


# Descriptor for enforcing values
class Unsigned(Descriptor):
    def __set__(self, instance, value):
        if value < 0:
            raise ValueError('Expected >= 0')
        super().__set__(instance, value)


class MaxSized(Descriptor):
    def __init__(self, name=None, **opts):
        if 'size' not in opts:
            raise TypeError('missing size option')
        super().__init__(name, **opts)

    def __set__(self, instance, value):
        if len(value) >= self.size:
            raise ValueError('size must be < ' + str(self.size))
        super().__set__(instance, value)

# Descriptor -> Typed      -> Integer   ->
#            -> Unsigned                -> UnsignedInteger
#            -> MaxSized

# -2^31 - 2^31-1
# 0 - 2^32
class Integer(Typed):
    expected_type = int

class UnsignedInteger(Integer, Unsigned):
    pass

class Float(Typed):
    expected_type = float

class UnsignedFloat(Float, Unsigned):
    pass

class String(Typed):
    expected_type = str

class SizedString(String, MaxSized):
    pass

class Stock:
    # Specify constraints
    name = SizedString('name', size=8)
    shares = UnsignedInteger('shares')
    price = UnsignedFloat('price')

    def __init__(self, name, shares, price):
        self.name = name
        self.shares = shares
        self.price = price

s = Stock("APPLE", 75, 10.0) # valid
print(s.name)
print(s.price)
# s1 = Stock("AAAAAAAAAAAA", 75, 10.0) #  size must be < 8
# s2 = Stock("ACME", -1, 10.0) # expect int
# s3 = Stock("ACME", 75, 10) # expect float

# Option2
# Class decorator to apply constraints
def check_attributes(**kwargs):
    def decorate(cls): # cls is the instance address
        print(cls)
        for key, value in kwargs.items():
            if isinstance(value, Descriptor):
                value.name = key
                setattr(cls, key, value) # https://www.programiz.com/python-programming/methods/built-in/setattr
            else:
                setattr(cls, key, value(key))
        return cls
    return decorate

# Example
@check_attributes(name=SizedString(size=8),
                  shares=UnsignedInteger,
                  price=UnsignedFloat)
class Stock2:
    def __init__(self, name, shares, price):
        self.name = name
        print(self.__dict__)
        self.shares = shares
        print(self.__dict__)
        self.price = price
        print(self.__dict__)

s2 = Stock2("ACME", 75, 10.0) # valid
print(s2.__dict__)
