# Lesson 24


#### Any question from last lesson?
1. https://www.kaggle.com/

## Classes
1. implementing a data model or type system
  1. review descriptor (see Lesson 22)
  1. we have \_\_set__ function but not \_\_get__ function in the class: if a descriptor will do nothing more than extract an named value, defining \_\_get__ is unneccessary.
  1. mixin class: when calling super, [which super we are calling](https://threadthehaystack.wordpress.com/2016/09/15/inheritance-super-and-mixins-in-python/)?
  1. alternative: class decoration(see: [more](https://realpython.com/primer-on-python-decorators/#decorating-classes))
1. implementing custom containers
  1. extends [collections](https://docs.python.org/3/library/collections.abc.html#module-collections.abc)
  1. implement the required functions
1. delegeting attribute access(skip)
1. defining more than one constructor in a class
  1. have method other than \_\_init__: [classmethod](https://www.programiz.com/python-programming/methods/built-in/classmethod)

## Hw
