import time

class Date:
    # Primary constructor
    def __init__(self, year, month, day):
        self.year = year
        self.month = month
        self.day = day

    # Alternate constructor
    @classmethod
    def today(cls):
        t = time.localtime()
        return cls(t.tm_year, t.tm_mon, t.tm_mday)

    def normal_method(self):
        pass

a = Date(2012, 12, 21) # Primary
a.normal_method()
b = Date.today() # Alternate
print(a.year)
print(b.year)

# work fine in inheritance
class NewDate(Date):
    pass

d = NewDate.today() # Creates an instance of NewDate (cls=NewDate)
print(d.year)
