# we have C, O, H elements and C has 4 links, O have 2 links and H has 1 links.
#
# C = O
# ||
# O
#
# is valid
#
# given elements mapping :{0: 'C', 1: 'O', 2: 'O'}
# and link [[0, 1], [0, 1], [0, 2], [0, 2]]

#  H - C = O
#      |
#      H
# is valid too.
#
# given elements mapping :{0: 'C', 1: 'O', 2: 'H', 3: 'H'}
# and link [[0, 1], [0, 2], [0, 3]]
# return if it's valid or not

from typing import List
def valid(mapping: dict, links: List[List[int]]):
    ...
