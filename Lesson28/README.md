# Lesson 28
Medians and Order statistics

#### Any question from last lesson?

## Medians and Order statistics
1. ith order statics: the ith smallest elements in n elements
  1. minimum: i = 1
  1. max: i = n
  1. median: i = (n+1) / 2 if n is odd and i = n/2 and n/2+1 when n is even
1. selection problem:
  1. input: a set A of n distinct numbers
  1. output: the element x that is larger than exactly i - 1 other elements of A.  
  1. naive solution: sort -> nlogn
1. minimun and maximum
  1. we need n-1 comparison => O(n)
  1. find the min and max at the same time
    1. first approach: keep min and max and compare => 2n - 2 comparison
    1. second: take two and compare, larger one compares with max and smaller one compares wit small => 3*(n/2)
1. how to solve the selction problem?
  1. randomized-select: like quick sort
  1. code
  1. worst case run time: O(n^2)
  1. average run time: O(n)
1. selection in worst-case linear time
  1. talk if we have time

## Hw
