# RANDOMIZED-SELECT returns the ith smallest element of the array A[p .. r].
from typing import List
# review https://runestone.academy/runestone/books/published/pythonds/SortSearch/TheQuickSort.html
def partition(A, p, r) -> int:
    x = A[r]
    i = p - 1
    for j in range(p, r):
        if A[j] <= x:
            i += 1
            A[i], A[j] = A[j], A[i]
    A[i+1], A[r] = A[r], A[i+1]
    return i+1

def select(A: List[int], p: int, r: int, i: int): # i start from 1
    if p == r: # i == 1
        return A[p]
    # q is the pivot_index
    q = partition(A, p, r)
    k = q - p + 1 # k == 6
    if i == k:
        return A[q]
    elif i < k:
        return select(A, p, q-1, i)
    else:
        return select(A, q+1, r, i-k) # why it's i-k? ()

def real_select(A, i):
    return select(A, 0, len(A)-1, i)

print(real_select([5, 3, 7, 0, 1, 4, 6, 2, 10], 4))
