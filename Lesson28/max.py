def find_max(A):
    if not A:
        print("error")
        return

    init = A[0]
    for i in A:
        init = max(A[i], init)
    return init
