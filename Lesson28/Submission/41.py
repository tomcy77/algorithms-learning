class Solution:
    def firstMissingPositive(self, nums) -> int:
        map = {}
        for n in nums:
            map[n] = 1
        i = 1
        while i in map:
            i += 1
        return i
print(Solution().firstMissingPositive([3,4,-1,1]))

your: O(kn) O(n)
mine: O(n)  O(n)*2
suggestion: O(n) O(n)
