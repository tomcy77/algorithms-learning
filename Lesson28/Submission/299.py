class Solution:
    def getHint(self, secret: str, guess: str) -> str:
        a, b = 0, 0
        guessed_g, guessed_s = [], []
        for i in range(len(guess)):
            if guess[i] == secret[i] and i not in guessed_s:
                a += 1
                guessed_g.append(i)
                guessed_s.append(i)
        for i in range(len(guess)):
            for j in range(len(secret)):
                if i != j and guess[i] == secret[j] and i not in guessed_g and j not in guessed_s:
                    b += 1
                    guessed_g.append(i)
                    guessed_s.append(j)
        return '{}A{}B'.format(a, b)

# map 10 -> O(n)
# O(n*n)
