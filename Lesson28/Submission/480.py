class Solution:
    def medianSlidingWindow(self, nums: List[int], k: int) -> List[float]:
        result = []
        window = sorted(nums[:k])
        for i in range(len(nums)-k+1):
            if k % 2 == 0:
                result.append((window[k//2]+window[k//2- 1])/2)
            else:
                result.append(window[k//2])
            window = sorted(nums[i+1:k+i+1])
        return result

#accepted but slow

# O(n*k*logk)
