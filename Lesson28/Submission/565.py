class Solution:
    def arrayNesting(self, nums: List[int]) -> int:
        max_length = 0
        for element in nums:
            length = 0
            if element != -1:
                max_length = max(max_length, self.findLoop(nums, element, length))
        return max_length
    def findLoop(self, nums, index, length):
        if nums[index] != -1:
            length += 1
            next_index = nums[index]
            nums[index] = -1
            return self.findLoop(nums, next_index, length)
        else:
            return length

#accepted
#once faced recursion problem, 14th line without return will return none type
