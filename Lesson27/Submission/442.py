# 442. Find All Duplicates in an Array

class Solution:
    def findDuplicates(self, nums: List[int]) -> List[int]:
        result = []
        for k in nums:
            if nums[abs(k)-1] < 0:
                result.append(abs(k))
            else:
                nums[abs(k)-1] *= -1
        return result
