# 706. Design HashMap

class MyHashMap:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.my_map = []

    def put(self, key: int, value: int) -> None:
        """
        value will always be non-negative.
        """
        for index, (k, v) in enumerate(self.my_map):
            if k == key:
                self.my_map[index] = (key, value)
                return
        self.my_map.append((key, value))


    def get(self, key: int) -> int:
        """
        Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key
        """
        for (k, v) in self.my_map:
            if k == key:
                return v
        return -1

    def remove(self, key: int) -> None:
        """
        Removes the mapping of the specified value key if this map contains a mapping for the key
        """
        for (k, v) in self.my_map:
            if key == k:
                self.my_map.remove((k, v))
                break

# Your MyHashMap object will be instantiated and called as such:
obj = MyHashMap()
obj.put(1,1)
obj.put(2,2)
print(obj.my_map)
print(obj.get(1))
print(obj.get(3))
obj.put(2,1)
print(obj.my_map)
# obj.remove(1)
