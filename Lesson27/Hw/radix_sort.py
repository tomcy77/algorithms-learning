from typing import List

def countSort(arr):
    # The output character array that will have sorted arr
    output = [[] for _ in arr]

    # Create a count array to store count of inidividul
    # characters and initialize count array as 0
    count = [0 for i in range(256)]

    # Store count of each character
    for (i, v) in arr:
        count[ord(i)] += 1
    # Change count[i] so that count[i] now contains actual
    # position of this character in output array
    for i in range(256):
        count[i] += count[i-1]
    # Build the output character array
    for i in range(len(arr)-1, -1, -1):
        output[count[ord(arr[i][0])]-1] = arr[i]
        count[ord(arr[i][0])] -= 1
    return [i[1] for i in output]

def sort(input: List[int]):
    str_input = [str(i) for i in input]
    max_length = 0
    for i in str_input:
        max_length = max(max_length, len(i))
    str_input = [i.zfill(max_length) for i in str_input]
    for i in range(max_length-1, -1, -1):
        str_input = countSort([(o[i], o) for o in str_input])
    return str_input

print(sort([125, 1, 123, 99, 124, 34,2354,4351]))
