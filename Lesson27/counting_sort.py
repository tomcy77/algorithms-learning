from typing import List

def sort(input: List[int], k: int) -> List[int]:
    result = []
    count = [0 for i in range(k)]
    for i in (input): # O(n)
        count[i] += 1
    for index, value in enumerate(count): # O(k)
        result.extend([index for i in range(value)])
    return result

print(sort([9, 3, 4, 6, 1, 1, 9, 9, 9], 10))













#
#
#
# def sort(n: List[int], k: int) -> List[int]:
#     count = [0 for i in range(k+1)]
#     for i in n:
#         count[i] += 1
#     result = []
#     for index, value in enumerate(count):
#         result.extend([index for i in range(value)])
#     return result
#
# print(sort([1, 1, 2, 5, 2, 2, 1, 4], 5))
