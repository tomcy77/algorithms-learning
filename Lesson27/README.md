# Lesson 27
Sorting in Linear Time

#### Any question from last lesson?

## Previous
1. Merge sort: upper bound is O(nlogn)
1. Heap sort: same as above
1. Quick sort: average time is O(nlogn)
1. Those algorithm are based on comparison sorts

## Counting Sort
1. we have n input with 0 to k range.
1. complexity: O(n)
1. code my version vs real version
1. [stable](http://notepad.yehyeh.net/Content/Algorithm/Sort/Sort.php): useful in radix sort

## Radix Sort
1. sort numbers with 10 digits
1. [sort from the least significant digit.](https://www.programiz.com/dsa/radix-sort)
1. stable sorting algorithm is important for solving the sub question.
1. Given n d-digit numbers where each digit has k possible values -> complexity: O(d(n+k))
1. when d is constant and k=O(n) -> complexity: O(n)

## Bucket Sort
1. assume A has n elements and each elements are in [0, 1]
1. we can have k bucket in [0, 1] and equally distributed. put each elements into the buckets and sort each bucket.
1. complexity: O(n) + sum(O(n_i^2)) where i from 0 to n-1 => O(n) if the sum of the squares of the bucket sizes is linear in the total number of elements.
1. maybe write code if we have time

## Hw
