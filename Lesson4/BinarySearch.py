# input is sorted

input = [1, 2, 3, 4, 5, 6, 7, 8]

def find(input, target):
    r = len(input) - 1
    l = 0
    while r >= l:
        mid = (r + l) // 2
        if input[mid] == target:
            return mid
        if input[mid] > target:
            r = mid - 1
        else:
            l = mid + 1
    return -1

print(find(input, 4))
print(find(input, 1))
print(find(input, 4.5))
print(find(input, 0))
print(find(input, 10))
