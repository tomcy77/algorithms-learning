# Given we have a linked list and the Node defined as the following.
# finish all required functions
class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
    # insert data into a particular position
    # sample input: data = 1 -> 2 -> 3 -> 4, target = 0, position: 0
    # samplet head node = 0 -> 1 -> 2 -> 3 -> 4
    def insert(self, target, position) -> Node:
        pass
    # append to the end of the original list
    def append(self, target):
        pass
    # remove based on position
    def remove(self, position) -> Node:
        pass
    # concate to another LinkedList
    def concat(self, others):
        pass
