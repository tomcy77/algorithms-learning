class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

# implement printTree function to print the tree structure to output
# for example:
# head = Node(1)
# head.left = Node(2)
# head.right = Node(3)
# when calling head.printTree()
# print out
#       1
#   2       3
#     5   6    7
#                 8
def printTree():
    return

# 104. Maximum Depth of Binary Tree
# Given a binary tree, find its maximum depth.
# The maximum depth is the number of nodes along the longest path from the root
# node down to the farthest leaf node.
# Note: A leaf is a node with no children.
def maxDepth(root: Node) -> int:
    pass

# 94. Binary Tree Inorder Traversal
# Given a binary tree, return the inorder traversal of its nodes' values.
def inorderTraversal(root: Node) -> List[int]:
    pass
