class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.left.left.right = Node(7)
root.left.right.left = Node(8)
root.left.right.right = Node(9)
root.right.left.right = Node(10)
# implement printTree function to print the tree structure to output
# for example:
# head = Node(1)
# head.left = Node(2)
# head.right = Node(3)
# when calling head.printTree()
# print out
#     1
# 2       3
4   5   6   7  2*3 - 1
def printTree(root: Node):
    return

# 104. Maximum Depth of Binary Tree
# Given a binary tree, find its maximum depth.
# The maximum depth is the number of nodes along the longest path from the root
# node down to the farthest leaf node.
# Note: A leaf is a node with no children.
def MaxDepth(root: Node):
    if not root :
        return 0
    else:
        depth = 1
        while root.right or root.left:
            if root.right:
                root = root.right
                depth += 1
            if root.left:
                root = root.left
                depth += 1
            # else :
            #     break
        return depth

# recursion
def MaxDepthRecursion(root: Node):
    if not root:
        return 0
    return max(MaxDepthRecursion(root.left), MaxDepthRecursion(root.right)) + 1
print(MaxDepthRecursion(root))
# 94. Binary Tree Inorder Traversal
# Given a binary tree, return the inorder traversal of its nodes' values.
tree = []
def InorderTraversal(root: Node):
    if root.left:
        InorderTraversal(root.left)
    tree.append(root.data)
    if root.right:
        InorderTraversal(root.right)
    return tree
print(InorderTraversal(root))

## Preorder
#tree = []
#def PreorderTraversal(root: Node):
#    tree.append(root.data)
#    if root.left:
#        PreorderTraversal(root.left)
#    if root.right:
#        PreorderTraversal(root.right)
#    return tree

## Postoreder
#tree = []
#def PostorderTraversal(root: Node):
#    if root.left:
#        PostorderTraversal(root.left)
#    if root.right:
#        PostorderTraversal(root.right)
#    tree.append(root.data)
#    return tree
