# Given we have a linked list and the Node defined as the following.
# finish all required functions
class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
    def printList(self):
        while self:
            print(self.data, end =" -> ")
            self = self.next
        print("\n")

    def length(self):
        if not self.next:
            return 1
        return 1 + self.next.length()
        # temp = []
        # while self :
        #     temp.append(self.data)
        #     self = self.next
        # return len(temp)
    def append(self, data):
        if self.next:
            self.next.append(data)
        if not self.next:
            self.next = Node(data)

    def insert(self, data, position):
        if position > self.length() or position < 0:
            print("Invalid position")
            return
        if position == 0:
            new_node = Node(data)
            new_node.next = self
            return new_node
        if position == self.length():
            self.append(data)
        else:
            cur = self
            while position > 1:
                cur = cur.next
                position -= 1
            new_node = Node(data)
            temp = cur.next
            cur.next = new_node
            new_node.next = temp
        return self
    def remove(self, position):
        if position == 0:
            return self.next
        cur = self
        while position > 1:
            cur = cur.next
            position -= 1
        cur.next = cur.next.next
        return self

    def concat(self, others_head): #also a class problem
        cur = self
        while cur.next:
            cur = cur.next
        cur.next = others_head

a = Node(1)
b = Node(2)
c = Node(3)
d = Node(4)
a.next = b
b.next = c
c.next = d

z = Node(10)
y = Node(20)
x = Node(30)
w = Node(40)
z.next = y
y.next = x
x.next = w
a.printList()
a.concat(z)
a.printList()
