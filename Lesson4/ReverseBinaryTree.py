class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

    def printTree():
        return

head = Node(1)
head.left = Node(2)
head.right = Node(3)
print(head.data)
print(head.left.data)
print(head.right.data)

def MirroringBinaryTree(root):
    if not root.left and not root.right:
        return
    if root.left and root.right:
        MirroringBinaryTree(root.right)
        MirroringBinaryTree(root.left)
        return
    if root.left:
        MirroringBinaryTree(root.left)
        root.right, root.left = root.left, None
    if root.right:
        MirroringBinaryTree(root.right)
        root.left, root.right = root.right, None

MirroringBinaryTree(head)
print(head.data)
print(head.left.data)
print(head.right.data)

















#
# def reverse(head):
#     if not head or (not head.left and not head.right):
#         return
#     if not head.left and head.right:
#         reverse(head.right)
#         head.left, head.right = head.right, None
#         return
#     if not head.right and head.left:
#         reverse(head.left)
#         head.right, head.left = head.left, None
#         return
#     reverse(head.left)
#     reverse(head.right)
#     head.left, head.right = head.right, head.left
#     return
#
# reverse(head)
# print(head.data)
# print(head.left.data)
# print(head.right.data)
