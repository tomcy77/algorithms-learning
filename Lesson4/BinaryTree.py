class BinaryNode:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

    def printTree():
        return

root = BinaryNode(1)
root.left = BinaryNode(2)
root.right = BinaryNode(3)

print(root.data)
print(root.left.data)
print(root.right.data)

def preorderTree(head, output):
    if head and head.left:
        preorderTree(head.left, output)
    if head:
        output.append(head.data)
    if head.right:
        preorderTree(head.right, output)
    return

output = []
preorderTree(root, output)
print(output)
