# Given we have a N * N matrix to represent roads
# 0 means we can not pass it
# 1 means we can pass it
# Given a starting point and end point, we can move to all four directions. (no returning)
# Find the longest path from start to end
import copy
mat = [
    [1, 0, 1, 1, 1, 1, 0, 1, 1, 1],
    [1, 0, 1, 0, 1, 1, 1, 0, 1, 1],
    [1, 1, 1, 0, 1, 1, 0, 1, 0, 1],
    [0, 0, 0, 0, 1, 0, 0, 1, 0, 0],
    [1, 0, 0, 0, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [1, 0, 0, 0, 1, 0, 0, 1, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 0, 1, 1],
    [1, 1, 0, 0, 1, 0, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 1, 0, 0]
]

def LongestPath(start, end, matrix, steps, max_step):
    start_x = start[0]
    start_y = start[1]
    if not ValidPoint(start_x, start_y, matrix):
        return
    if start_x == end[0] and start_y == end[1] and steps > max_step[0]:
        max_step[0] = steps
        return
    four_direcion = [[0, 1], [0, -1], [1, 0], [-1, 0]]
    matrix[start_x][start_y] = -1
    for step in four_direcion:
        LongestPath([start_x + step[0], start_y + step[1]], end, matrix, steps + 1, max_step)
    matrix[start_x][start_y] = 1
    return

def ValidPoint(x, y, matrix):
    if x >= len(matrix) or x < 0 or y >= len(matrix[0]) or y < 0:
        return False
    if matrix[x][y] == -1 or matrix[x][y] == 0:
        return False
    return True

result = [0]
LongestPath([0, 0], [5, 7], mat, 0, result)
print(result[0])
