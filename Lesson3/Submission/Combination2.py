# same as Combination
# sample input: ['a', 'a', 'c']
# no repeat result like ['a', 'a', 'c'] and ['a', 'a', 'c']
# sample output: [['a', 'a', 'c'], ['a', 'c', 'a'], ['c', 'a', 'a']]
# don't take set after calling Combination

def Comb(input):
    #sorting part
    for i in range(len(input)-1):
        for j in range(i+1, len(input)):
            if input[i] == input[j]:
                input[i+1], input[j] = input[j], input[i+1]
    #combining part
    output = []
    if len(input) <= 1:
        return [input]
    if len(input) == 2 and input[0] == input[1]: #checking part will retrun nothing in this situation
        return input
    for i, element in enumerate(input):
        if element == input[i-1]: #duplicate checking part
            break
        for j in Comb(input[:i] + input[i+1:]):
            output.append([element]+j)
    return output
print(Comb(['a', 'b', 'c']))