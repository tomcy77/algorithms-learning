# define class
List = [1, 2, 3, 4]
class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
    def printList(self):
        if self.next:
            print("%s" % self.data + " -> ", end="")
            self.next.printList()
        else:
            print(self.data)

one = Node(1) # instance of Node
two = Node(2) # instance of Node
three = Node(3)
one.next = three
one.printList()
