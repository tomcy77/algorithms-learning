# input = [a, b, c]
# output = [[a,b,c], [a,c,b], [b,a,c], [b,c,a], [c,a,b], [c,b,a]]
# return all combinations

def combinations(input):
    if len(input) == 0:
        return []
    if len(input) == 1:
        return [input] # not returning input
    output = []
    for i in range(len(input)):
        cur = input[i] # take b
        elseElements = input[:i] + input[i+1:] # [a] + [c] => [a, c]
        for combi in combinations(elseElements):
            print("take ", cur)
            print("attach with ", combi)
            output.append([cur] + combi)
    return output
print(combinations(['a', 'a', 'c']))






























# def Combination(input):
#     output = []
#     if not input:
#         return []
#     if len(input) == 1:
#         return [input]
#     for i in range(len(input)):
#         cur = input[i]
#         for c in Combination(input[:i] + input[i+1:]):
#             output.append([cur] + c)
#     return output
#
# print(Combination(['a', 'b', 'c']))
# print(Combination(['a', 'a', 'c']))
