# same as Combination
# sample input: ['a', 'a', 'c']
# no repeat result like ['a', 'a', 'c'] and ['a', 'a', 'c']
# sample output: [['a', 'a', 'c'], ['a', 'c', 'a'], ['c', 'a', 'a']]
# don't take set after calling Combination

# first a -> recur([a, c])
# second a -> recur([a, c])

def Comb(input):
    #sorting part
    input = sorted(input)
    #combining part
    output = []
    if len(input) <= 1:
        return [input]
    for i, element in enumerate(input):
        if i != 0 and element == input[i-1]: #duplicate checking part
            continue
        for j in Comb(input[:i] + input[i+1:]):
            output.append([element]+j)
    return output

print(Comb(['a', 'a']))
print(Comb(['a', 'a', 'c']))
print(Comb(['a', 'b', 'c'])) # input size = 3 => output size = 3! = 6
# input size = n => output size = n!
