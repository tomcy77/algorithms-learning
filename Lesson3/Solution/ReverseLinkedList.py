# Write a function to reverse a linked list.
# For example, 1 -> 2 -> 3 -> 4
# after running the function, it becomes 4 -> 3 -> 2 -> 1
# class NodeList:
#     def __init__(self, list):
#         self.list = list
#     def reverse(self):
#         pass
#
class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

head = Node(1)
head.next = Node(2)
head.next.next = Node(3)

def reverse(head):
    if not head.next:
        return head, head
    otherNodeHead, otherNodeLast = reverse(head.next)
    otherNodeLast.next = head
    return otherNodeHead, head

reversed, _ = reverse(head)
print(reversed.data)
print(reversed.next.data)
print(reversed.next.next.data)

# 1 -> 2 -> 3 -> 4
# 1 call recursion on (2 -> 3 -> 4) => (4 -> 3 -> 2) return 4, 2
# 4 3 2 -> 1 return 4, 1
