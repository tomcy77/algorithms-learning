# Given a Sudoku game with init setup, try to find one possible solution.
# We can assume we can always find one.

input = [
[-1, 8, 9, -1, 5, -1, -1, -1, 6],
[-1, 2, -1, 4, -1, 6, -1, -1, -1],
[-1, -1, 5, 7, -1, 8, 3, -1, -1],
[6, -1, -1, 2, 8, -1, -1, 3, 4],
[2, 3, -1, -1, -1, -1, -1, 6, 5],
[5, 9, -1, -1, 6, 4, -1, -1, 7],
[-1, -1, 6, 8, -1, 9, 2, -1, -1],
[-1, -1, -1, 6, -1, 7, -1, 8, -1],
[8, -1, -1, -1, 4, -1, 6, 7, -1]
]

def SudokuSolver(metrix):
    no_negative_one = True
    for i in range(9):
        for j in range(9):
            if metrix[i][j] == -1:
                no_negative_one = False
    if no_negative_one:
        return True
    for i in range(9):
        for j in range(9):
            if metrix[i][j] == -1:
                for k in range(1, 10): # (i = 0, j = 0, k = 1)
                    metrix[i][j] = k
                    if valid(metrix, i, j) and SudokuSolver(metrix):
                        return True
                metrix[i][j] = -1
                return False

def valid(matrix, a, b):
    # i => (0, 2), j = (0, 2) => (0, 0) -> (2, 2)
    # i // 3, j // 3 => *3 + 0 + 1+2
    cur = matrix[a][b]
    for i in range(9):
        if i != b and matrix[a][i] == cur:
            return False
    for i in range(9):
        if i != a and matrix[i][b] == cur:
            return False
    aa = a // 3
    bb = b // 3
    for i in range(3*aa, 3*aa+3):
        for j in range(3*bb, 3*bb+3):
            if i != a and j != b and matrix[i][j] == cur:
                return False
    return True

print(SudokuSolver(input))
print(input)


# [[3, 8, 9, 1, 5, 2, 7, 4, 6],
#  [1, 2, 7, 4, 3, 6, 5, 9, 8],
#  [4, 6, 5, 7, 9, 8, 3, 1, 2],
#  [6, 7, 1, 2, 8, 5, 9, 3, 4],
#  [2, 3, 4, 9, 7, 1, 8, 6, 5],
#  [5, 9, 8, 3, 6, 4, 1, 2, 7],
#  [7, 4, 6, 8, 1, 9, 2, 5, 3],
#  [9, 5, 3, 6, 2, 7, 4, 8, 1],
#  [8, 1, 2, 5, 4, 3, 6, 7, 9]]
