# determine the input is Palindrome or not

[1, 2, 1]

def isPalindrome1(input):
    if not input:
        return True
    for i in range(int(len(input) / 2) + 1):
        if input[i] == input[len(input) - i - 1]:
            continue
        else:
            return False
    return True

def isPalindrome(input):
    if not input:
        return True
    if input[0] == input[-1]:
        return isPalindrome(input[1:-1])
    return False

print(isPalindrome("123"))
print(isPalindrome("121"))
print(isPalindrome("1221"))
