# Lesson3
LinkedList and some recursion problems.

#### Any question from last lesson?
* complexity
* merge sort: NlogN

## LinkedList
1. Linked List: Node with data and next. (LinkedList.py)
1. Can be useful when inserting and removing.

## Problem set
1. Check Palindrome (Palindrome.py)
1. Print all possible combinations from a set of n characters (Combination.py)

## Hw
