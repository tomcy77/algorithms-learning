# Lesson 21

#### Any question from last lesson?
1. format
  1. print('The date is {:ymd}'.format(d)) in format_example.py
1. Functions and classes

## Classes
1. Making Objects Support the Context-Management Protocol
  1. with statement
  1. implement \_\_enter__() and \_\_exit__()
  1. help to manage resource and memory
1. Saving Memory When Creating a Large Number of Instances
  1. functions of [\_\_dict__](https://docs.python.org/3/library/stdtypes.html#object.__dict__)
  1. [add \_\_slots__ in class](https://stackoverflow.com/questions/472000/usage-of-slots)
  1. faster access and memory save
  1. only use when you need to create the class for a lot of instances
1. Encapsulate “private” data on instances of a class
  1. there is no access control in Python
  1. use _ and __ to differenciate it
  1. use __ when the method should not be overrided
  1. use _ after variable name to avoid reserved key words like for, while
1. add extra processing (e.g., type checking or validation) to the getting or
setting of an instance attribute.
  1. getter, setter and deleter

## Hw
