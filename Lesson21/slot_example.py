import sys

class DateSimpleClass:
    def __init__(self, year, month, day):
        self.year = year
        self.month = month
        self.day = day

date = DateSimpleClass(2020, 12, 18)
print(date.__dict__)
date.second = 12
print(date.__dict__)
print("Memory comsumption of dic is {} bytes.".format(sys.getsizeof(date.__dict__)))

class DateSlotClass:
    __slots__ = ['year', 'month', 'day']
    def __init__(self, year, month, day):
        self.year = year
        self.month = month
        self.day = day
date2 = DateSlotClass(2020, 12, 18)
# print(date2.__dict__)
# print(date2.__slots__)
# date2.second = 10
print("Memory comsumption of DateSlotClass is {} bytes.".format(sys.getsizeof(date2)))
