class A:
    def __init__(self):
        self._internal = 0 # An internal attribute
        self.public = 1 # A public attribute

    def public_method(self):
        '''
        A public method
        '''
        pass

    def _internal_method(self):
        pass

a = A()
a.public_method()
# you can still access the method with underscore
a._internal_method()

# double underscores
class Animal:
    def __init__(self):
        self.test = 100
        self.__private = 0

    def __private_method(self):
        print('call B private method')
        pass

    def public_method(self):
        print('call B public method.')

class Dog(Animal):
    def __init__(self):
        super().__init__()
        self.test = 200
        self.__private = 1 # Does not override B.__private

    def public_method(self):
        print("Dog public")
    # Does not override B.__private_method()
    def __private_method(self):
        print('call C private method')
        pass

dog = Dog()
dog._Dog__private_method()
dog._Animal__private_method()
print(dog.__dict__)
# put underscore after for avoiding reserved words
for_ = 'variable for'
while_ = 'variable while'

def main(_):
    pass
