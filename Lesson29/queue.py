

class Queue:
    def __init__(self, n):
        self.head = 0
        self.tail = 0
        self.array = [0 for _ in range(n)]

    def enqueue(self, x): #O(1)
        self.array[self.tail] = x
        self.tail += 1
        if self.tail == len(self.array):
            self.tail = 0

    def dequeue(self): #O(1)
        x = self.array[self.head]
        self.head += 1
        if self.head == len(self.array):
            self.head = 0
        return x

q = Queue(3)
q.enqueue(1)
q.enqueue(2)
q.enqueue(3)
print(q.array)
print(q.dequeue())
print(q.array)
q.enqueue(4)
print(q.array)
