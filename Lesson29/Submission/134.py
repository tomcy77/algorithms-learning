class Solution:
    def canCompleteCircuit(self, gas: List[int], cost: List[int]) -> int:
        n = len(gas)
        for i in range(n):
            gas_left = gas[i] - cost[i]
            if gas_left >= 0:
                counter = 0
                while counter != n and gas_left >= 0:
                    counter += 1
                    if i+counter < n:
                        gas_left += gas[i+counter] - cost[i+counter]
                    else:
                        gas_left += gas[i+counter-n] - cost[i+counter-n]
                if gas_left >= 0:
                    return i
        return -1
