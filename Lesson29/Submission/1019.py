# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def nextLargerNodes(self, head: ListNode) -> List[int]:
        result = []
        origin_head = origin_headhead
        list_ = []
        while head.next:
            list_.append(head)
            while list_ and list_[-1].val < head.next.val:
                list_[-1].val = head.next.val
                list_.pop()
            head = head.next
        head.val = 0
        while list_:
            list_[-1].val = 0
            list_.pop()
        while origin_head:
            result.append(origin_head.val)
            origin_head = origin_head.next
        return result
