# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def swapNodes(self, head: ListNode, k: int) -> ListNode:
        counter = 0
        left_head, right_head = head, head
        while counter != k-1: #find the kth node
            left_head = left_head.next
            counter += 1
        new_head = left_head
        while new_head.next: #down to end
            counter += 1
            new_head = new_head.next
        for i in range(counter-k+1): #down to -kth node
            right_head = right_head.next
        left_head.val, right_head.val = right_head.val, left_head.val
        return head
