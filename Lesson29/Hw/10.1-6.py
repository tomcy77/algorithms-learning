# Show how to implement a queue using two stacks.
# Analyze the running time of the queue operations.
class Stack:

    def __init__(self, n):
        self.array = []
        self.size = n

    def push(self, x): #O(1)
        if self.top() == self.size:
            print("Stack Overflow")
        else:
            self.array.append(x)

    def pop(self): #O(1)
        if self.is_empty():
            print("Stack Underflow")
        else:
            x = self.array[-1]
            del self.array[-1]
            return x

class Queue:
    def __init__(self, n):
        ...

    def enqueue(self, x):
        ...

    def dequeue(self):
        ...

q = Queue(3)
q.enqueue(1)
q.enqueue(2)
q.enqueue(3)
print(q.array)
print(q.dequeue())
print(q.array)
q.enqueue(4)
print(q.array)
