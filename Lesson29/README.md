# Lesson 29
Data Structures

#### Any question from last lesson?
Medians and Order statistics

## Chapter
1. Chapter 10: Elementary Data Structures: simple data structures such as stacks, queues, linked lists, and rooted trees
1. Chapter 11: Hash Tables
1. Chapter 12: Binary Search Trees  
1. Chapter 13: Red-Black Trees: a variant of binary search trees
1. Chapter 14: Augmenting Data Structures: augment red-black trees to support operations other than the
basic ones listed above

## Introduction
1. dynamic: the sets manipulated by algorithms can grow, shrink, or otherwise change over time
1. elements of dynamic sets: each element is represented by an object whose fields can be examined and manipulated if we have a pointer to the object
1. Operations on dynamic sets:
  1. queries: return information about the set
    1. SEARCH(S, k)
    1. MINIMUM(S)
    1. MAXIMUM(S)
    1. SUCCESSOR(S, x)
    1. PREDECESSOR(S, x)
  1. modifying operations: change the set
    1. INSERT(S, x)
    1. DELETE(S, x)

## Elementary Data Structures
1. stack
  1. last in, first out (LIFO)
  1. push: insert one element into stack
  1. pop: delete one element from stack
  1. top(S): indicate the top index
  1. stack_empty(S): whether the stack is empty or not
  1. If an empty stack is popped, we say the stack underflows, which is normally an error. If top[S] exceeds n, the stack overflows
1. queue
  1. first in, first out (FIFO)
  1. enqueue: insert
  1. dequeue: delete
  1. has head and tail to record the current status; n elements
  1. overflow and underflow
1. linked list
  1. single linked list vs double linked list
  1. circular list: the prev pointer of the head of the list points to the tail, and the next pointer
of the tail of the list points to the head
  1. LIST-SEARCH(L, k): finds the first element with key k in list L by a simple linear search -> O(n)
  1. LIST-INSERT(L, x): insert in the front -> O(1)
  1. LIST-DELETE(L, x): removes an element x from a linked list L -> O(1) but we need to search first O(n)
  1. the benefit of dummy head and tail
  1. write code if we have time

## Hw
