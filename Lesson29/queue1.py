import unittest

class Queue:
    def __init__(self, n):
        self.array = []
        self.size = n

    def enqueue(self, x):
        if self.size == len(self.array):
            print('overflow')
        else:
            self.array.append(x)
    def dequeue(self):
        if len(self.array) > 0:
            element = self.array[0]
            del self.array[0] #O(n)??
            return element
        print("underflow")


class TestStringMethods(unittest.TestCase):
    def test_queue_withOneOperation(self):
        q = Queue(10)
        q.enqueue(1)
        self.assertEqual(q.dequeue(), 1)

    def test_queue_withTwoOperation(self):
        q = Queue(2)
        q.enqueue(1)
        q.enqueue(2)
        self.assertEqual(q.dequeue(), 1)
        self.assertEqual(q.dequeue(), 2)

if __name__ == '__main__':
    unittest.main()
