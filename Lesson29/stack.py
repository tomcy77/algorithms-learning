class Stack:

    def __init__(self, n):
        self.array = []
        self.size = n

    def push(self, x): #O(1)
        if self.top() == self.size:
            print("Stack Overflow")
        else:
            self.array.append(x)

    def pop(self): #O(1)
        if self.is_empty():
            print("Stack Underflow")
        else:
            x = self.array[-1]
            del self.array[-1]
            return x

    def top(self):
        return len(self.array)

    def is_empty(self):
        return len(self.array) == 0

s = Stack(10)
s.push(1)
s.push(2)
print(s.pop())
print(s.pop())
print(s.pop())




















#
#
# # Each of the three stack operations takes O(1) time.
# def top(S):
#     return len(S)
#
# def stack_empty(S):
#     if top(S) == 0:
#         return True
#     return False
#
# def push(S, x):
#     S.append(x)
#
# def pop(S):
#     if stack_empty(S):
#         print("underflow")
#     else:
#         del S[top(S)-1]
#
# S = []
# push(S, 1)
# push(S, 2)
# pop(S)
# pop(S)
# pop(S)
