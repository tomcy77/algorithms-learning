
def sort(input):
    if not input: # len(input) == 0
        return []
    if len(input) == 1:
        return input
    first = input[0]
    smaller = []
    bigger = []
    for i in range(1, len(input)):
        if input[i] > first:
            bigger.append(input[i])
        else:
            smaller.append(input[i])
    return sort(smaller) + [first] + sort(bigger)

input = [9, 4, 5, 1, 10, 6]
# smaller = [4, 5, 1, 6] => 4, [1], [5, 6] => 5, [], [6]
# bigger = [10]
print(sort(input))























#
# def Merge(input):
#     if not input or len(input) == 1:
#         return input
#     first = input[0]
#     larger = []
#     smaller = []
#     for element in input[1:]:
#         if first > element:
#             smaller.append(element)
#         else:
#             larger.append(element)
#     return Merge(smaller) + [first] + Merge(larger)
#
# input = [9, 4, 5, 1, 10, 6]
#
# print(Merge([9, 4, 5, 1, 10, 6]))
# print(Merge([9, 4, 5, 1, 10, 6, 7, 6]))
