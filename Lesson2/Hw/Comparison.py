# We know the worst case running time of insertion sort and merge sort are n^2
# and nlogn respectly. Try to create a large random array to prove that Merge
# Sort is actaully running better than Insertion sort (or bubble sort or
# selection sort).
#
# some uesful tools:
# (timeit)[https://docs.python.org/2/library/timeit.html]
# (random)[https://docs.python.org/3/library/random.html]
#
# compare different size of input and the result. Try to make a graph of it (by
# hand is ok).
