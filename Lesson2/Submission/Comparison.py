# We know the worst case running time of insertion sort and merge sort are n^2
# and nlogn respectly. Try to create a large random array to prove that Merge
# Sort is actaully running better than Insertion sort (or bubble sort or
# selection sort).
#
# some uesful tools:
# (timeit)[https://docs.python.org/2/library/timeit.html]
# (random)[https://docs.python.org/3/library/random.html]
#
# compare different size of input and the result. Try to make a graph of it (by
# hand is ok).
import time
import random

input = [random.randint(-100, 100)for i in range(10)
start_time = time.time()
def MergeSort(input):
    if not input or len(input) == 1:
        return input
    anchor = input[0]
    big = []
    small = []
    for i in range(1,len(input)):
        if anchor > input[i]:
            small.append(input[i])
        else:
            big.append(input[i])
    return MergeSort(small) + [anchor] + MergeSort(big)
print(MergeSort(input))
end = time.time() - start
print(end)


def sort(input):
    input = [random.randint(-100, 100)for i in range(10)]
    output = []
    for element in input:
        if(len(output) == 0):
            output.append(element)
        else:
            for i in range(len(output)):
                cur = output[i]
                if(element < cur):
                    output.insert(i, element)
                    break
                elif(element > cur and i == len(output)-1):
                    output.append(element)
                    break
                else:
                    continue
    return output
