# In class discussion, we split the array to subarray by the first element
# which is not ideal. Try to implement the correct merge sort by split the
# array into equal or similar size subarray

# merge sort
# seperate problems by small pieces

input = [9, 4, 5, 1, 10, 6]
#
# sub_input1 = [9, 4, 5] sub_input2 = [1, 10, 6]
# sorted_sub1 = [4, 5, 9] sorted_sub2 = [1, 6, 10]

# merge = [1, 4, 5, 6, 9, 10]
import copy

def MergeSort(input):
    if not input or len(input) == 1: # no input or sorting not needed
        return input
    mid = int(len(input) / 2)
    left = MergeSort(input[:mid])
    right = MergeSort(input[mid:])
    output = []
    while right or left:
        if not right:
            output.extend(left)
            break
        if not left:
            output.extend(right)
            break
        if left[0] > right[0] :
            output.append(right[0])
            right.remove(right[0])
        else :
            output.append(left[0])
            left.remove(left[0])
    return output

print(MergeSort(input))
