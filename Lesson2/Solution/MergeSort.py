# In class discussion, we split the array to subarray by the first element
# which is not ideal. Try to implement the correct merge sort by split the
# array into equal or similar size subarray

def MergeSort(input):
    if not input or len(input) == 1:
        return input
    mid = int(len(input) / 2)
    return CombineList(MergeSort(input[:mid]), MergeSort(input[mid:]))

def CombineList(list1, list2):
    index1 = 0
    index2 = 0
    output = []
    while index1 < len(list1) and index2 < len(list2):
        if list1[index1] < list2[index2]:
            output.append(list1[index1])
            index1 += 1
        else:
            output.append(list2[index2])
            index2 += 1
    if index1 < len(list1):
        output.extend(list1[index1:])
    if index2 < len(list2):
        output.extend(list2[index2:])
    return output
