import MergeSort
import random
import timeit

def InsertionSort(input):
    output = []
    for element in input:
        if(len(output) == 0):
            output.append(element)
        else:
            for i in range(len(output)):
                cur = output[i]
                if(element < cur):
                    output.insert(i, element)
                    break
                elif(element > cur and i == len(output)-1):
                    output.append(element)
                    break
                else:
                    continue
    return output

def TestMergeSort():
    SETUP_CODE = '''
from __main__ import MergeSort
import random'''
    TEST_CODE = '''
input = [random.randint(-1000, 1000)for i in range(3000)]
MergeSort.MergeSort(input)'''

    times = timeit.timeit(setup = SETUP_CODE,
                          stmt = TEST_CODE,
                          number = 100)

    print('Merge sort time: {}'.format(times))

TestMergeSort()

def TestInsersionSort():
    SETUP_CODE = '''
from __main__ import InsertionSort
import random'''
    TEST_CODE = '''
input = [random.randint(-1000, 1000)for i in range(3000)]
InsertionSort(input)'''

    times = timeit.timeit(setup = SETUP_CODE,
                          stmt = TEST_CODE,
                          number = 100)

    print('Insertion sort time: {}'.format(times))
TestInsersionSort()
