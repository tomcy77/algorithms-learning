input = [9, 4, 5, 1, 10, 6]

# def sort(input):
#     output = []
#     for element in input:
#         print(output)
#         if len(output) == 0:
#             output.append(element)
#             continue
#         for i in range(len(output)):
#             if i == len(output) - 1 and output[i] < element:
#                 output.append(element)
#                 break
#             if i == 0 and element < output[0]:
#                 output.insert(0, element)
#                 break
            # if output[i] < element < output[i+1]:
            #     output.insert(i+1, element)
            #     break
#
#     return output
#
# print(sort(input))

# input size is n.

def sort(input):
    output = [] # 1 * c0
    for element in input: # n times in c1
        if(len(output) == 0): # n times in c2
            output.append(element) # 1 times in c3
        else:
            for i in range(len(output)): # 1 + 2 + 3 ... + (n-1) = k times in c4
                cur = output[i] # k times in c5
                if(element < cur): # k times in c6
                    output.insert(i, element) # n1 times in c7
                    break
                elif(element > cur and i == len(output)-1): # smaller than k times in c8
                    output.append(element) # n - n1 - 1 times in c9
                    break
                else:
                    continue
    return output

input2 = [1]
print(sort(input2))

# n * (c1 + c2) + c3 + k * (c4 + c5 + c6 + c8) + (n-1) * (c7 + c9)
# best case: if k is constant => C1 * n + C2 => linear times
# worst case: if k is 1 + 2 + 3 ... + n-1 = (n) * (n-1) / 2 => C1 * n^2 + C2 * n + C3 => quadratic function
