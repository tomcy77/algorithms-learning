# Lesson2
Analyze the algorithm and Merge sort.

#### Any question from last lesson?
* insertion sort
* git usage and any tool problem?

## Analyze the algorithm
1. The algorithm can be measured by different ways like memory usage, communication bandwidth.
1. Computation time: generic one-processor, random-access machine ([RAM](https://en.wikipedia.org/wiki/Random-access_machine)). no concurrent execution.
1. Most of the calculation takes constant time.
1. Analyze the insertion sort. (AnalyzeInsertionSort.py)
1. Worst case vs Best case vs Average case (normally as bad as Worst case).
1. Rate of growth (order of growth): Assume n is large enough.
1. discuss TwoSum problem(failed in last test case since timeout).
  1. Insertion sort: N^2 algorithm vs Merge function or quick sort: NlogN
## Merge sort
1. recursion (divide and conquer)
  1. Divide
  1. Conquer
  1. Combine
1. Fibonacci Sequence (FibonacciSequence.py)
1. Merge sort
  1. when length is 1 or 0, we don't need to do anything (sorted)
  1. split the array into two subarray (smaller problem)
  1. sort the subarray
  1. merge two sorted subarray
1. The computation time is nlogn which is better than insertion sort.

## Hw
