# Fibonacci Sequence
# 0 1 1 2 3 5 8 13 21 34

# def findFibonacci1(index):
#     input = [0, 1]
#     for i in range(index):
#         input.append(input[-1] + input[-2])
#     print(input[-1])

def findFibonacci2(index):
    if index == 0:
        return 0
    if index == 1:
        return 1
    return findFibonacci2(index - 1) + findFibonacci2(index - 2)
print(findFibonacci2(9))
