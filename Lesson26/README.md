# Lesson 26


#### Any question from last lesson?
Testing, Mock, Exceptions

## Testing
1. Logging Test Output to a File
  1. unittest.main()
1. Skip or ignore some unittests
  1. @unittest.skip
  1. @unittest.skipIf
  1. @unittest.skipUnless
  1. @unittest.expectedFailure
  1. the decorator can be applied to class also.

## Exceptions
1. Handling Multiple Exceptions
  1. try and except

## Hw
