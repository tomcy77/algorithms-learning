import sys
import unittest

def main(out=sys.stderr, verbosity=2):
    # https://docs.python.org/3/library/unittest.html#unittest.TestLoader
    loader = unittest.TestLoader()
    suite = loader.loadTestsFromModule(sys.modules[__name__])
    unittest.TextTestRunner(out,verbosity=verbosity).run(suite)

if __name__ == '__main__':
    with open('testing.out', 'w') as f:
        main(f)
