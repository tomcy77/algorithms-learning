import numpy as np
# feel free to refer to https://en.wikipedia.org/wiki/Linear_congruential_generator
class Node:
    def __init__(self, key, value):
        self.data = (key, value)
        # self.key = key
        # self.value = value
        self.next = None

class HashTable:
    def __init__(self, size):
        self.seed = 200
        self.array = [None for _ in range(size)]
        self.size = size
        self._mapping = {}
        
    def get(self, key):
        index = self._hash_function(key)
        ele = self.array[index]
        while ele:
            if ele.data[0] == key:
                return ele.data[1]
            else:
                ele = ele.next
        return 'Given Key Has No Corresponded Value'

    def insert(self, key, value):
        index = self._hash_function(key)
        
        if self.array[index] == None:
            self.array[index] = Node(key, value)
        else:
            ele = self.array[index]
            while ele:
                if ele.data[0] == key:
                    ele.data = (key, value)
                ele = ele.next
            ele = Node(key, value)
            print(ele.data)
        return self.array[index]
    
    def delete(self, key):
        index = self._mapping(key)
        ele = prev = self.array[index]
        if not ele:
            return 'Given Key Does Not Exist'
        if ele.data[0] == key:
            self.array[index] = ele.next
        else:
            ele = ele.next
            while ele:
                if ele.data[0] == key:
                    prev.next = ele.next
                    break
                ele, prev = ele.next, prev.next
        

    def _hash_function(self, key):
        if key not in self._mapping:
            self._mapping[key] = self._get_random() % self.size
            
        return self._mapping[key]

    def _get_random(self):
        new_one = (self.seed * 22695477 + 1) % 2 ** 32
        self.seed = new_one
        return new_one

my_hash = HashTable(3)
h1 = my_hash.insert(1, 10)
print(h1.data)
if(h1.next):
    print(h1.next.data)
h2 = my_hash.insert(2, 20)
print(h2.data)
if(h2.next):
    print(h2.next.data)
h3 = my_hash.insert(3, 30)
print(h3.data)
if(h3.next):
    print(h3.next.data)
h4 = my_hash.insert(4, 40)
print(h4.data)
if(h4.next):
    print(h4.next.data)
# question 2: prove the hashing table is efficient?
# uniform distribution

