import numpy as np
# feel free to refer to https://en.wikipedia.org/wiki/Linear_congruential_generator
class Node:
    def __init__(self, value):
        self.next = None
        ...

class HashTable:
    def __init__(self, size):
        self.array = [np.nan for _ in range(size)]

    def get(key):
        ...

    def insert(key, value):
        ...

    def delete(key):
        ...

    def _hash_function(key):
        ...

    def _get_random(key):
        ...

# question 2: prove the hashing table is efficient?
# uniform distribution
