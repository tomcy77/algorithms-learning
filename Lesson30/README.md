# Lesson 30


#### Any question from last lesson?

## Hash tables
1. Definition
  1. support dictionary operations such as INSERT, SEARCH and DELETE.
  1. stored data in key-value pairs where each key is associated with a particular value.
  1. U: universe
1. Use array as hash table
  1. Hash function
  1. Collision
1. [direct-address table](https://www.kindsonthegenius.com/introduction-to-hash-tables-and-direct-addressing/)
  1. small number of keys
  1. no two elements have the same key
  1. DIRECT-ADDRESS-SEARCH(T, k): return T [k]
  1. DIRECT-ADDRESS-INSERT(T, x): T[key[x]] ← x
  1. DIRECT-ADDRESS-DELETE(T, x): T[key[x]] ← NIL
  1. each operation is O(1)
1. Problem of direct address table
  1. ??1
  1. ??2
1. Hash function
  1. compute the slot from key k
  1. h: U -> {0, 1, ..., m-1}
  1. if two keys hash to the same slot => collision
  1. solution: randomized, chaining, open addressing
1. Chaining
  1. if two keys map to the same slot -> make slot a linked list
  1. see graph in the books
  1. CHAINED-HASH-INSERT(T, x): insert x at the head of list T[h(key[x])]
  1. CHAINED-HASH-SEARCH(T, k): search for an element with key k in list T[h(k)]
  1. CHAINED-HASH-DELETE(T, x): delete x from the list T[h(key[x])]
  1. complexity discussion
1. Good hash functions
  1. each key is equally likely to hash to any of the m slots
  1. but we don't know the population
  1. if the key is not natural number -> convert it to natural number
  1. example: p is 112 and t = 116 in ASCII -> 112*128 + 116 = 14452
  1. division method
    1. mod: h(k) = k mod m
    1. fast (only mod function)
    1. A prime not too close to an exact power of 2 is often a good choice for m
  1. multiplication method
    1. step 1: multiply the key k by a constant A in the range 0 < A < 1 and extract the fractional part of kA
    1. step 2: multiply this value by m and take the floor of the result
    1. advantage: m is not important, bit operation
  1. universal hashing
    1. choose the hash function randomly in a way that is independent of the keys that are actually going to be stored
    1. radom number -> mod (see lesson 14 hw)

## Hw

??1: too many possible key
??2: waste memory
