# Lesson 5
Heap and Heap sort

#### Any question from last lesson?
1. Binary tree
1. Binary search

## Heap sort
1. merge sort (nlogn) vs insertion sort (constant space) => heap sort
1. what is heap? a nearly complete binary tree in array format. (HeapExample.py)
1. max-heap (heapsort) vs min-heap (piority queue)
1. heap property: for each node i other than the root -> A[parent(i)] >= A[i]
1. height of the tree: log(n) (2^? = N => ? = logN)
1. [MAX-HEAPIFY](http://staff.ustc.edu.cn/~csli/graduate/algorithms/book6/chap07.htm): take log(n)
1. BUILD-MAX-HEAP: linear time(n)
1. HEAPSORT procedure, which runs in nlogn
1. HeapSort.py

## Hw
