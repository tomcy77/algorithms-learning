# 110. Balanced Binary Tree
# Given a binary tree, determine if it is height-balanced.
#
# For this problem, a height-balanced binary tree is defined as:
#
# a binary tree in which the left and right subtrees of every node differ in
# height by no more than 1.
# Example 1:
#
# Given the following tree [3,9,20,null,null,15,7]:
#
#     3
#    / \
#   9  20
#     /  \
#    15   7
# Return true.
#
# Example 2:
#
# Given the following tree [1,2,2,3,3,null,null,4,4]:
#
#        1
#       / \
#      2   2
#     / \
#    3   3
#   / \
#  4   4
# Return false.

# Definition for a binary tree node.
class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.left.left.right = Node(7)
root.left.right.left = Node(8)
root.left.right.right = Node(9)
root.right.left.right = Node(10)

#              1
#          /       \
#        2           3
#      /   \       /
#    4       5    6
#      \    /  \    \
#       7  8     9   10


# my last version of MaxDepth was wrong
def MaxDepth(root: Node) -> bool:
    if not root:
        return 0
    return max(MaxDepth(root.left), MaxDepth(root.right)) + 1


def IsBalanced(root: Node):
    if not root:
        return True
    if not root.left and not root.right:
        return True
    Ldepth = MaxDepth(root.left)
    Rdepth = MaxDepth(root.right)
    if abs(Ldepth - Rdepth) >= 2:
        return False
    if not IsBalanced(root.left) or not IsBalanced(root.right):
        return False
    return True

print(IsBalanced(root))