# 1367. Linked List in Binary Tree
# https://leetcode.com/problems/linked-list-in-binary-tree/
# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def isSubPath(head: ListNode, root: TreeNode) -> bool:
    if head.val == root.val:
        if not head.next:
            return True
        if root.left and head.next.val == root.left.val:
            if isSubPath(head.next, root.left):
                return True
        if root.right and head.next.val == root.right.val:
            if isSubPath(head.next, root.right):
                return True
    if root.left and isSubPath(head, root.left):
        return True
    if root.right and isSubPath(head, root.right):
        return True
    return False

# def traverseTree(root, prefix, output):
#     if not root.right and not root.left:
#         output.append(prefix + str(root.val))
#     if root.left:
#         traverseTree(root.left, prefix + str(root.val), output)
#     if root.right:
#         traverseTree(root.right, prefix + str(root.val), output)
#     return
#
def isSubPath(head: ListNode, root: TreeNode) -> bool:
    result = []
    traverseTree(root, "", result)
    link = []
    while head:
        link.append(str(head.val))
        head = head.next
    target = ''.join(link)
    for s in result:
        if target in s:
            return True
    return False
# def isSubPath(head: ListNode, root: TreeNode) -> bool:

a = ListNode(1)
b = ListNode(2)
c = ListNode(3)
d = ListNode(4)
a.next = b
b.next = c
c.next = d

T = TreeNode(1)
T.left = TreeNode(2)
T.left.left = TreeNode(3)
T.left.left.left = TreeNode(4)

root = TreeNode(1)
root.left = TreeNode(2)
root.right = TreeNode(3)
root.left.left = TreeNode(4)
root.left.right = TreeNode(5)
root.right.left = TreeNode(6)
root.left.left.right = TreeNode(7)
root.left.right.left = TreeNode(8)
root.left.right.right = TreeNode(9)
root.right.left.right = TreeNode(10)

output = []
traverseTree(root, "", output)
print(output)
