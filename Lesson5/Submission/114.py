# 114. Flatten Binary Tree to Linked List
# Given a binary tree, flatten it to a linked list in-place. (Preorder)
#
# For example, given the following tree:
#
#     1
#    / \
#   2   5
#  / \   \
# 3   4   6
# The flattened tree should look like:
#
# 1
#  \
#   2
#    \
#     3
#      \
#       4
#        \
#         5
#          \
#           6
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
        
tree = []
def RightTraversal(root: TreeNode):
    tree.append(root.val)
    if root.right:
        RightTraversal(root.right)
    return tree

def flatten(root): 
    if not root:
        return        
    if root.left and root.right:                
        flatten(root.left)
        flatten(root.right)
        
        head = root
        end = root.right
        root.right = root.left
        root.left = None
        
        while head.right:           
            head = head.right                  
        head.right = end
        
    if root.left:              
        flatten(root.left)
        root.right = root.left
        root.left = None                
    if root.right:
        flatten(root.right)

def flatten_helper(root):
    if not root.left and not root.right:
        return root, root
    if root.left and root.right:
        left_head, left_end = flatten_helper(root.left)
        right_head, right_end = flatten_helper(root.right)
        root.right = left_head
        left_end.right = right_head
        right_head.left = None
        right_end.left = None
        left_end.left = None
        left_head.left = None
        root.left = None
        return root, right_end
    if root.left:
        left_head, left_end = flatten_helper(root.left)
        root.right = left_head
        left_end.left = None
        left_head.left = None
        root.left = None
        return root, left_end
    if root.right:
        _, end = flatten_helper(root.right)
        return root, end


root = TreeNode(1)
root.left = TreeNode(2)
root.right = TreeNode(3)
root.left.left = TreeNode(4)
root.left.right = TreeNode(5)
root.right.left = TreeNode(6)
root.left.left.right = TreeNode(7)
root.left.right.left = TreeNode(8)
root.left.right.right = TreeNode(9)
root.right.left.right = TreeNode(10)

flatten(root)
print(RightTraversal(root))