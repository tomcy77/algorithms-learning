def Left(i):
    return 2 * i + 1

def Right(i):
    return 2 * i + 2

def Parent(i):
    return (i-1) // 2

def Heapify(A, lenA, i): # O(logN)
    l = Left(i)
    r = Right(i)
    if l < lenA and A[l] > A[i]:
        largest = l
    else:
        largest = i
    if r < lenA and A[r] > A[largest]:
        largest = r
    if largest != i:
        print("Swap ", i, " and ", largest, " with vaule = ", A[i], " and ", A[largest])
        A[largest], A[i] = A[i], A[largest]
        Heapify(A, lenA, largest)

# input = [16, 4, 10, 14, 7, 9, 3, 2, 8, 1]
# Heapify(input, len(input), 1)
# print(input)

def BuildHeap(A):
    for i in range(len(A) // 2, -1, -1): #N/2 * log N = NlogN
        print("Build from ", i)
        Heapify(A, len(A), i)

# input = [4, 1, 3, 2, 16, 9, 10, 14, 8, 7]
# BuildHeap(input)
# print(input)

def HeapSort(A):
    BuildHeap(A) # N/2 * logN
    lenA = len(A)
    for i in range(len(A)-1, 0, -1): #N-1
        A[0], A[i] = A[i], A[0]
        lenA -= 1
        print("After swap ", A)
        Heapify(A, lenA, 0)  # logN


# N/2 * logN + (N-1) * logN = NlogN

input = [16, 14, 10, 8, 7, 9, 3, 2, 4, 1]
HeapSort(input)
print(input)
