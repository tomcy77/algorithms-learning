input = [1, 2, 3, 4, 5, 6, 7]
 index   1, 2, 3, 4, 5, 6, 7

# root index = 1
# parent(i) = i//2
# left(i) = 2i  => leaf
# right(i) = 2i+1 => leaf

            1
        2       3
    4     5    6     7
