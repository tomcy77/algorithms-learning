# assign function to a variable
def list_sum(demo_list):
    return sum(demo_list)

income_list = [1000,1378.90,3456,89.780]
total_income = list_sum

print(total_income(income_list))
#output: 5924.679999999999

# option 2: nested
def list_sum(demo_list):
    def sum_integers():
        return sum([_ for _ in demo_list if isinstance(_, int)])
    return sum_integers()

income_list = [1000,1378.90,3456,89.780]
print(list_sum(income_list))
#output: 4456

# return a function from a function
def list_sum(demo_list):
    def sum_integers():
        return sum([_ for _ in demo_list if isinstance(_, int)])
    return sum_integers

income_list = [1000,1378.90,3456,89.780]
ss = list_sum(income_list)
print(ss)
print(ss())

#output: <function list_sum.<locals>.sum_integers at 0x7fb4756906a8>
from typing import List
# pass function to another function as input
def list_sum(demo_list: List[float]) -> float:
    return sum(demo_list)

def sum_income(func):
    income_list = [1000,1378.90,3456,89.780]
    return func(income_list)

print(sum_income(list_sum))

#Output: 5924.679999999999

# decoration
# def get_fullname(firstname: str, lastname: str) -> str:
#     return "FirstName: {}, LastName: {}".format(lastname, firstname)

def capitalize_names(func):
    def func_wrapper(fname, lname):
        return func(fname, lname).upper()
    return func_wrapper
#
# get_fullname = capitalize_names(get_fullname)
# print(get_fullname('Rahman', 'Solanke'))

#Output: SOLANKE, RAHMAN

# is the same as
@capitalize_names
def get_fullname(firstname, lastname):
    return "{}, {}".format(lastname, firstname)

print(get_fullname('Rahman', 'Solanke'))
