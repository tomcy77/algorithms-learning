class lazyproperty:
    def __init__(self, func):
        self.func = func

    def __get__(self, instance, _notused):
        if instance is None:
            return self
        else:
            value = self.func(instance)
            setattr(instance, self.func.__name__, value)
            return value

import math

class Circle:
    # self.__dict__
    def __init__(self, radius):
        self.radius = radius

    @lazyproperty
    def area(self):
        print('Computing area')
        return math.pi * self.radius ** 2

    @lazyproperty
    def perimeter(self):
        print('Computing perimeter')
        return 2 * math.pi * self.radius

    # https://amir.rachum.com/blog/2019/10/16/descriptors/
    # call this one first if found -> call area or perimeter if not
    def __getattribute__(self, name):
        print('call __getattribute__ with {}'.format(name))
        return 100

c = Circle(4.0)
# print(c.radius)
print(c.area)
print(c.area)
# print(c.perimeter)
# print(c.perimeter)
#
# # take a deeper look
# c = Circle(4.0) # Get instance variables
# print(vars(c))
# print(c.area) # Compute area and observe variables afterward
# print(c.area)  # Notice access doesn't invoke property anymore
# print(vars(c))
# # difference bwtween del and delete (https://www.geeksforgeeks.org/python-__delete__-vs-__del__/)
# del c.area # Delete the variable and see property trigger again
# print(vars(c))
# print(c.area)
#
# one drawback is that the value can be changed
# print(c.area)
# c.area = 25
# print(c.area)
#
# # fix
#
def lazyproperty2(func):
    name = '_lazy_' + func.__name__
    @property
    def lazy(self):
        if hasattr(self, name):
            return getattr(self, name)
        else:
            value = func(self)
            setattr(self, name, value)
            return value
    # @lazy.setter
    # def lazy(self, value):
    #     print('call setter')
    return lazy

class Circle2:
    def __init__(self, radius):
        self.radius = radius

    @lazyproperty2
    def area(self):
        print('Computing area')
        return math.pi * self.radius ** 2

    @lazyproperty2
    def perimeter(self):
        print('Computing perimeter')
        return 2 * math.pi * self.radius

# c = Circle2(4.0)
# print(vars(c))
# print(c.area)
# c.area = 25
