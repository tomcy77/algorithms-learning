# 367. Valid Perfect Square
# Given a positive integer num, write a function which returns True if num is a
# perfect square else False.
#
# Follow up: Do not use any built-in library function such as sqrt.
#
#
#
# Example 1:
#
# Input: num = 16
# Output: true
# Example 2:
#
# Input: num = 14
# Output: false

# O(sqrt(N))
class Solution:
    def isPerfectSquare(self, num: int) -> bool:
        i = 1
        while num > 0:
            num -= i
            i += 2
        return num == 0

# 1 3 4 5....10
# O(log(N))
# 2^N = Num
# N log(2) = log(Num)
class Solution:
    def isPerfectSquare(self, num: int) -> bool:
        left = 1
        right = num
        while left <= right:
            mid = (left + right) // 2
            square = mid * mid
            if square == num:
                return True
            if square < num:
                left = mid + 1
            else:
                right = mid - 1
        return False
