# 581. Shortest Unsorted Continuous Subarray
# Given an integer array nums, you need to find one continuous subarray that if
# you only sort this subarray in ascending order, then the whole array will be
# sorted in ascending order.
#
# Return the shortest such subarray and output its length.
#
#
#
# Example 1:
#
# Input: nums = [2,6,4,8,10,9,15]
# Output: 5
# Explanation: You need to sort [6, 4, 8, 10, 9] in ascending order to make the
# whole array sorted in ascending order.
# Example 2:
#
# Input: nums = [1,2,3,4]
# Output: 0
# Example 3:
#
# Input: nums = [1]
# Output: 0

# O(N log N)
# nums =   [2, 6, 8, 4, 10, 9, 15]
# sorted = [2, 4, 6, 8, 9, 10, 15]

class Solution:
    def findUnsortedSubarray(self, nums: List[int]) -> int:
        sorted_nums = sorted(nums)
        left = 0
        while left < len(nums):
            if nums[left] == sorted_nums[left]:
                left += 1
            else:
                break
        if left == len(nums):
            return 0
        right = len(nums) - 1
        while right > 0:
            if nums[right] == sorted_nums[right]:
                right -= 1
            else:
                break
        return right - left + 1
