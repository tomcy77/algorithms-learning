# Lesson 23
Merry Chrismas

#### Any question from last lesson?

## Classes
1. [decorator](https://medium.com/@solankerahman/custom-decorators-in-python-d4ed0747e533)
  1. function can be assigned to a variable
  1. a function can return another function
  1. decorator is the syntactic sugar
1. Using Lazily Computed Properties
  1. cache the result and we don't need to calculate each time
  1. use decorator to defind the lazy function
  1. \_\_delete__ is used to delete the attribute of an instance
  1. \_\_del__ is a destructor method which is called as soon as all references of the object are deleted
1. Simplifying the Initialization of Data Structures
  1. use heritence
1. Defining an Interface or Abstract Base Class
  1. what is [Interface and abstract class](https://www.guru99.com/interface-vs-abstract-class-java.html)?
  1. abc module
  1. not to overuse it since Python is a dynamic language that gives you great flexibility.
  
## Hw
