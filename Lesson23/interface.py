from abc import ABCMeta, abstractmethod

class IStream(metaclass=ABCMeta):
    @abstractmethod
    def read(self, maxbytes=-1):
        pass

    @abstractmethod
    def write(self, data):
        pass

# Can't instantiate abstract class
# a = IStream()

class SocketStream(IStream):
    def read(self, maxbytes=-1):
        print("read")

    def write(self, data):
        print('write')

class BStream(IStream):
    def read(self, maxbytes=-1):
        print("read")

    def write(self, data):
        print('write')

def serialize(obj, stream):
    if not isinstance(stream, IStream):
        raise TypeError('Expected an IStream')
    read = stream.read()


# implement the abstract class by registering
import io

# Register the built-in I/O classes as supporting our interface
IStream.register(io.IOBase)

# Open a normal file and type check
f = open('foo.txt')
isinstance(f, IStream) # Returns True
