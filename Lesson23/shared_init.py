import math

class Structure1:
    # Class variable that specifies expected fields
    _fields = []

    def __init__(self, *args):
        if len(args) != len(self._fields):
            raise TypeError('Expected {} arguments'.format(len(self._fields)))
        # Set the arguments
        for name, value in zip(self._fields, args):
            setattr(self, name, value)

# Example class definitions
class Stock(Structure1):
    _fields = ['name', 'shares', 'price']

class Point(Structure1):
    _fields = ['x', 'y']

class Circle(Structure1):
    _fields = ['radius']

    def area(self):
        return math.pi * self.radius ** 2

# s = Stock('ACME', 50, 91.1)
# print(s)
# p = Point(2, 3)
# print(p)
# c = Circle(4.5)
# print(c)
# s2 = Stock('ACME', 50)

# support keyword args
class Structure2:
    _fields = []

    def __init__(self, *args, **kwargs):
        if len(args) > len(self._fields):
            raise TypeError('Expected {} arguments'.format(len(self._fields)))

        # Set all of the positional arguments
        for name, value in zip(self._fields, args):
            setattr(self, name, value)

        # Set the remaining keyword arguments
        for name in self._fields[len(args):]:
            setattr(self, name, kwargs.pop(name))

        # Check for any remaining unknown arguments
        if kwargs:
            raise TypeError('Invalid argument(s): {}'.format(','.join(kwargs)))

class Stock2(Structure2):
    _fields = ['name', 'shares', 'price']

s1 = Stock2('ACME', 50, 91.1)
s2 = Stock2('ACME', 50, price=91.1)
s3 = Stock2('ACME', shares=50, price=91.1)
# s4 = Stock2('ACME', shares=50, price=91.1, aa=1)
