def count(n):
    while True:
        yield n
        n += 1

c = count(0)
# print(c[10:20])

# use itertools.islice
import itertools
for x in itertools.islice(c, 10, 20):
    print(x)

# the status will be kept and continue when calling next.
print(next(c))
