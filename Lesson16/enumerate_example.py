my_list = ['a', 'b', 'c']
# for idx, val in enumerate(my_list):
#     print(idx, val)

# specify the starting index
for idx, val in enumerate(my_list, 1):
    print(idx, val)

data = [ (1, 2), (3, 4), (5, 6), (7, 8) ]

# Correct!
for n, (x, y) in enumerate(data):
    print(n, x, y)
# Error!
for n, x, y in enumerate(data):
    print(n, x, y)
