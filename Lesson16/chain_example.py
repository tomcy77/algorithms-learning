from itertools import chain

a = [1, 2, 3, 4]
b = ['x', 'y', 'z']
for x in chain( [1, 2, 3], "xyz"):
    print(x)

# another option, inefficient
for x in [1, 2, 3] + "xyz":
    print(x)
