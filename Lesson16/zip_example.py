xpts = [1, 5, 4, 2, 10, 7]
ypts = [101, 78, 37, 15, 62, 99]
for x, y in zip(xpts, ypts):
    print(x,y)

# if different length, return with the shortest one
a = [1, 2, 3]
b = ['w', 'x', 'y', 'z']
for i in zip(a,b):
    print(i)

# if you want to iterate throught the longest one, use itertools.zip_longest()
from itertools import zip_longest
for i in zip_longest(a,b):
    print(i)

# if can also specify the value if None
for i in zip_longest(a, b, fillvalue=0):
    print(i)
#
# zip can also accept multiple inputs.
# zip will return a iterator, make it a list if you want to keep it
print(zip(a, b))
print(list(zip(a, b)))
