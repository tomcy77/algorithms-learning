from itertools import permutations

items = ['a', 'b', 'c']
for p in permutations(items):
    print(p)

# specify the length
for p in permutations(items, 2):
    print(p)
#
# combinations
from itertools import combinations
for c in combinations(items, 3):
    print(c)
#
# specify length
for c in combinations(items, 2):
    print(c)
#
for c in combinations(items, 1):
    print(c)
