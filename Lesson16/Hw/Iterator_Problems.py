# Problem 1: Write an iterator class reverse_iter, that takes a list and
# iterates it from the reverse direction.
# naive way:
# def reverse_iter(input):
#     list_of_input = list(input)
#     for i in list_of_input[::-1]:
#         yield i

def reverse_iter(input):
    pass

# it = reverse_iter([1, 2, 3, 4])
# next(it)
# return 4
# next(it)
# return 3
# next(it)
# return 2
# next(it)
# return 1
# next(it)
# throw StopIteration exception


# Problem 2: Write a program that takes one or more filenames as arguments and
# prints all the lines which are longer than 40 characters.

def print_line_with_40_more(*files):
    pass

# Problem 3: Write a function findfiles that recursively descends the directory
# tree for the specified directory and generates paths of all the files in the tree.

def findfiles(root):
    pass

# Problem 4: Write a function to compute the number of python files (.py extension)
# in a specified directory recursively.

def count_python_files(root):
    pass

# Problem 5: Write a function to compute the total number of lines of code
# in all python files in the specified directory recursively.

def find_total_lines(root):
    pass

# Problem 6: Write a function to compute the total number of lines of code,
# ignoring empty and comment lines, in all python files in the specified
# directory recursively.

def find_total_lines_without_empty(root):
    pass

# Problem 7: Write a program split.py, that takes an integer n and a filename
# as command line arguments and splits the file into multiple small files with
# each having n lines.

def split_files(file_path, n):
    pass
