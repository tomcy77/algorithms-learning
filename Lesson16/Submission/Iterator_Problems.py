# Problem 1: Write an iterator class reverse_iter, that takes a list and iterates it from the reverse direction.

def reverse_iter(input):
    input.reverse()
    return iter(input)
a = [1, 2, 3]
a = reverse_iter(a)
# print(next(a))

# Problem 2: Write a program that takes one or more filenames as arguments and
# prints all the lines which are longer than 40 characters.

def print_line_with_40_more(*files):
    for f in files:
        infile = open(f, 'r')
        for i in infile:
            if len(i) >= 40:
                print(i, end = (''))
# print_line_with_40_more('./Iterator_Problems.py', './README.md')

# Problem 3: Write a function findfiles that recursively descends the directory
# tree for the specified directory and generates paths of all the files in the tree.

import os
def findfiles(root):
    result = []
    for dp, dn, filenames in os.walk(root):
        for f in filenames:
            result.append(os.path.join(dp, f))
    return result
# for i in findfiles('..\\'):
#     print(i)


# Problem 4: Write a function to compute the number of python files (.py extension)
# in a specified directory recursively.

def list_python_files(root):
    paths = findfiles(root)
    return [path for path in paths if path.endswith('.py')]
# print(list_python_files('..\\'))

# Problem 5: Write a function to compute the total number of lines of code
# in all python files in the specified directory recursively.

def find_total_lines(root):
    counter = 0
    for f in list_python_files(root):
        infile = open(f, 'r')
        for _ in infile:
            counter += 1
    return counter
# print(find_total_lines('.\\..\\'))

# Problem 6: Write a function to compute the total number of lines of code,
# ignoring empty and comment lines, in all python files in the specified
# directory recursively.

def find_total_lines_without_empty(root):
    infile = open(files, 'r')
    counter = 0
    for i in infile:
        if i[0] != '#' and i[0] != '\n':
            counter += 1
    return counter


# Problem 7: Write a program split.py, that takes an integer n and a filename
# as command line arguments and splits the file into multiple small files with
# each having n lines.
from collections import deque
def split_files(file_path, n):
    times = find_total_lines(file_path)//n
    infile = open(file_path, 'r')
    lines = deque(infile.readlines())
    for i in range(times):
        for j in range(n):
            create = open('newfile_{}.txt'.format(i+1), 'w')
            create.close()
            write = open('newfile_{}.txt'.format(i+1), 'a')
            write.write(lines.popleft())
            write.close()
split_files('\\README.md', n)
