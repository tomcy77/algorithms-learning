# Lesson 16
Iterator

#### Any question from last lesson?
1. Iterator vs generator:
  1. generator is a iterator but iterator is not neccessarly a generator.
  1. iterator: \_\_next__ and \_\_iter__
  1. iterable: \_\_iter__ and [\_\_getitem__](https://stackoverflow.com/questions/43627405/understanding-getitem-method)

## iterator
1. defining generator functions with extra state.
1. slice on generator: [itertools.islice()](https://docs.python.org/2/library/itertools.html#itertools.islice)
1. skip iterator elements by conditions: [itertools.dropwhile()](https://docs.python.org/2/library/itertools.html#itertools.dropwhile)
  1. once the skip condition is fulfilled, no test anymore
1. list all combinations of a set of elements: [itertools.permutations()](https://docs.python.org/2/library/itertools.html#itertools.permutations) and [itertools.combinations()](https://docs.python.org/2/library/itertools.html#itertools.combinations)
1. enumerate(): iterate through elements and index
1. zip(): iterate with multiple iterable
1. itertools.chain(): chain multiple iterables

## Hw
