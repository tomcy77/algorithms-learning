text = """##
# User Database
#
# Note that this file is consulted directly only when the system is running
# in single-user mode. At other times, this information is provided by
# Open Directory.
##
nobody:*:-2:-2:Unprivileged User:/var/empty:/usr/bin/false
root:*:0:0:System Administrator:/var/root:/bin/sh"""

lines = text.split('\n')
# for line in lines:
#     print(line)

# skip the comments
from itertools import dropwhile
for line in dropwhile(lambda line: line.startswith('#'), lines):
    print(line)

# if you know how many you want to skip, use itertool.islice()
from itertools import islice
items = ['a', 'b', 'c', 1, 4, 10, 15]
for x in islice(items, 3, None):
    print(x)
