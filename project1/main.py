# export FLASK_APP=main.py
# flask run
from flask import Flask, request, render_template
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField, FieldList, FormField
import util
import os
from flask import flash

app = Flask(__name__)
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY
app.config['TEMPLATES_AUTO_RELOAD'] = True

class SubDirForm(FlaskForm):
    subDirName = TextField(validators=[DataRequired()])

class SubDirsForm(FlaskForm):
    subDirList = FieldList(FormField(SubDirForm), min_entries=0)
    submit = SubmitField(label='Print')

class Sample(FlaskForm):
    name = StringField()
    flist = FieldList(TextField())
    submit = SubmitField('Submit')

@app.route("/step1")
def home():
    user_subdirs = {}
    form = SubDirsForm(subdirs = user_subdirs)
    flash("You're are now live!")
    return render_template('index.html', title=('Home'), form=form)

@app.route('/step1', methods=['POST'])
def my_form_post():
    text = request.form['text']
    processed_text = util.calculate(text)
    return processed_text

# @app.route('/step2')
# def my_form_post():
#     pass
#
# @app.route('/step2', methods=['POST'])
# def my_form_post():
#     pass

if __name__ == '__main__':
    app.run(debug = True)
