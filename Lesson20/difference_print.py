class Pair:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return 'Pair({}, {})'.format(self.x, self.y)

    def __str__(self):
        return '({0.x}, {0.y})'.format(self)

p = Pair(3, 4)
p # call __repr__
print(p) # call __str__

print('p is {!r}'.format(p)) # !r means using __repr__ to replace __str__
print('p is {}'.format(p))
#
# repr examples
def __repr__(self):
    return 'Pair({0.x!r}, {0.y!r})'.format(self) # 0 == self
# or
def __repr__(self):
    return 'Pair(%r, %r)'.format(self.x, self.y)
