# Lesson 20

#### Any question from last lesson?
1. \*, **
1. type
1. default parameter

## Functions
1. anonymous functions: lambda
  1. limitation: multiple statements, conditionals, iterations, exception handling
  1. lambda with variables: the variables are determined in runtime
1. reduce function variables: partial
1. Simplify single method class to functions
1. Some more complex examples: skipped

## Classes and Objects
1. changing the string representation of instances
  1. \_\_str__() and \_\_repr__()
1. customizing string formatting
  1. \_\_format__()

## Hw
