add = lambda x, y: x + y
print(add(2,3))
print(add('hello', 'world'))

# same as
def add2(x, y):
    return x + y

# typical examples
names = ['David Beazley', 'Brian Jones',
'Raymond Hettinger', 'Ned Batchelder']

s = sorted(names, key=lambda name: name.split()[-1].lower())
print(s)

# the function to return a function
def myfunc(n: int):
  return lambda x : x * n

mydoubler = myfunc(2)
print(mydoubler(10))

mytripler = myfunc(3)
print(mytripler(10))
