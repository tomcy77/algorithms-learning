x = 10
a = lambda y: x + y
x = 20
b = lambda y: x + y

print(a(10))
print(b(10))

# the x is deteremined in run time
x = 15
print(a(10))
x = 3
print(a(10))
#
# if you want to fix the variable => make it default parameters
x = 10
a = lambda y, x=x: x + y
x = 20
b = lambda y, x=x: x + y
print(a(10))
print(b(10))
#
# wrong example
funcs = [lambda x: x+n for n in range(5)]
for f in funcs:
    print(f(0))
#
# correct
funcs = [lambda x, n=n: x+n for n in range(5)]
for f in funcs:
    print(f(0))
