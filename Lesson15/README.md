# Lesson 15

#### Any question from last lesson?
1. date and iterator(delegating iteration)
1. \_private_variable vs [\_\_private_variable](https://stackoverflow.com/questions/1301346/what-is-the-meaning-of-single-and-double-underscore-before-an-object-name)

## Iteration
1. yield can transform a function into iterator
1. [geneartor vs iterator](https://anandology.com/python-practice-book/iterators.html)
  1. iteration protocal: iter and next
  1. generator: a function that produces a sequence of results instead of a single value
  1. generator is a iterator
1. custom generator example and associated iterator class
1. iterating in Reverse

## Hw
