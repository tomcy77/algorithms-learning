def foo():
    print("begin")
    for i in range(3):
        print("before yield", i)
        yield i
        print("after yield", i)
    print("end")

f = foo()
print(next(f))
# begin
# before yield 0
# 0
print(next(f))
# # after yield 0
# # before yield 1
# # 1
print(next(f))
# # after yield 1
# # before yield 2
# # 2
try:
    print(next(f))
except StopIteration:
    print("end iteration")
# # after yield 2
# # end
