def frange(start, stop, increment):
    x = start
    while x < stop:
        yield x
        x += increment

# you can put this into any funciont which accepts iterable
for n in frange(0, 4, 0.5):
    print(n)
# or sum
print(sum(frange(0, 4, 0.5)))

# the detailed execution
def countdown(n):
    print('Starting to count from', n)
    while n > 0:
        yield n
        n -= 1
    print('Done!')
c = countdown(3)
print(c)
print(next(c))
print(next(c))
print(next(c))
print(next(c))
