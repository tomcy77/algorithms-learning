import requests
import random
import string

def randStr(chars = string.ascii_letters + string.digits, N=5):
	return ''.join(random.choice(chars) for _ in range(N))

url = 'https://mega-scripts.xyz/processing.php?token=f145ad6149cadb487d2c4f53bd6b58ea'
counter = 0
while 1:
	counter += 1
	myobj = {'exampleInputEmail1': randStr(N=6), 'exampleInputPassword1': randStr(N=12)}
	print("{}: {}".format(counter, myobj))
	x = requests.post(url, data = myobj)
	print(x.status_code)
