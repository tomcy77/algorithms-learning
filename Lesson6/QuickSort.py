from typing import List

def partition(A, p, r):
    x = A[r]
    i = p - 1
    for j in range(p, r):
        if A[j] <= x:
            i += 1
            A[i], A[j] = A[j], A[i]
    A[i+1], A[r] = A[r], A[i+1]
    return i+1

A = [2, 8, 7, 1, 3, 5, 6, 4]
# print(partition(A, 0, 7))
# print(A)
#
def quick_sort(A: List[int], p: int, r: int):
    if p < r:
        pivot_index = partition(A, p, r)
        quick_sort(A, p, pivot_index - 1)
        quick_sort(A, pivot_index + 1, r)
quick_sort(A, 0, len(A) - 1)
print(A)
