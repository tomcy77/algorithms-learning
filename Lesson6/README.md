# Lesson 6
QuickSort

#### Any question from last lesson?
1. Heap
1. Heap sort

## Quick sort
1. Based on divide and conquer
  1. Devide: partition the array A[p r] to A[p q-1] and A[q+1 r]. (we need to compute index q)
  1. Conquer: sort the two subarrays A[p q-1] and A[q+1 r] by recursion.
  1. Combine: no action
  1. QuickSort.py
1. Complexity analysis
  1. n log n
  1. Worst case analysis: T(n) = T(n-1) + T(0) + O(n) = T(n-1) + O(n)
   => T(n) = O(n^2)
  1. Best case analysis: T(n) <= 2T(n/2) + O(n) => T(n) = O(n log n) [note](https://cathyatseneca.gitbooks.io/data-structures-and-algorithms/content/analysis/notations.html)
  1. Balanced partition: if 9 to 1 split, T(n) <= T(9n/10) + T(n/10) + cn. T(n) = nlogn
1. Randomized Quicksort
  1. swap random element with the last one
  1. Best algorithm for sorting given a large enough input
## Hw
