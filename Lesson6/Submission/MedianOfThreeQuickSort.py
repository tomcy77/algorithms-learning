# One way to improve the RANDOMIZED-QUICKSORT procedure is to partition around a
# pivot that is chosen more carefully than by picking a random element from the
# subarray. One common approach is the median-of-3 method: choose the pivot as
# the median (middle element) of a set of 3 elements randomly selected from the
# subarray.

import random

def choose(A, index_start, index_end):
    three = [A[random.randint(index_start, index_end)],
    A[random.randint(index_start, index_end)],
    A[random.randint(index_start, index_end)]]
    min_of_three = min(three)
    max_of_three = max(three)
    three.remove(min_of_three)
    three.remove(max_of_three)
    return A.index(three[0])

def partition(A, p, r):
    temp = choose(A, p, r)
    x = A[temp]
    A[r], A[temp] = A[temp], A[r]
    i = p - 1
    for j in range(p, r):
        if A[j] <= x:
            i += 1
            A[i], A[j] = A[j], A[i]
    A[i+1], A[r] = A[r], A[i+1]
    return i+1

def quick_sort(A, p, r):
    if p < r:
        pivot_index = partition(A, p, r)
        quick_sort(A, p, pivot_index - 1)
        quick_sort(A, pivot_index + 1, r)

A = [2, 8, 7, 1, 3, 5, 6, 4]
quick_sort(A, 0, len(A) - 1)
print(A)
