# 19. Remove Nth Node From End of List
#
# Given a linked list, remove the n-th node from the end of list and return
# its head.
#
# Example:
#
# Given linked list: 1->2->3->4->5, and n = 2.
#
# After removing the second node from the end, the linked list becomes
# 1->2->3->5.
# Note:
# Given n will always be valid.

class ListNode:
    def __init__(self, data):
        self.val = data
        self.next = None
    def printList(self):
        while self:
            print(self.val, end ="")
            self = self.next
    def removeNthFromEnd(self, head, n):
        temp = head
        # put linked list in a list to measure the length of linked list
        list = []
        a = head
        while a.next:
            list.append(a.val)
            a = a.next
        list.append(a.val)
        #when n is the first node of linked list
        if n == len(list):
            return(head.next)
        #other cases, find the node before n and link it to the node next to n
        for _ in range(len(list)-n-1):
            head = head.next
        head.next = head.next.next
        return(temp)

a = ListNode(1)
b = ListNode(2)
c = ListNode(3)
d = ListNode(4)
a.next = b
b.next = c
c.next = d

a.removeNthFromEnd(a, 3)
a.printList()
