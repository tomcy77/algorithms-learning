# 3. Longest Substring Without Repeating Characters
# Given a string, find the length of the longest substring without repeating
# characters.
#
# Example 1:
#
# Input: "abcabcbb"
# Output: 3
# Explanation: The answer is "abc", with the length of 3.
# Example 2:
#
# Input: "bbbbb"
# Output: 1
# Explanation: The answer is "b", with the length of 1.
# Example 3:
#
# Input: "pwwkew"
# Output: 3
# Explanation: The answer is "wke", with the length of 3.
#              Note that the answer must be a substring, "pwke" is a subsequence
#              and not a substring.
# "abc" find a in "abc" or not
# list => a -> b -> c => N
# set => 1
# dict: key => value
# set: key

class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        if len(s) == 0:
            return 0
        if len(s) == 1:
            return 1

        max_length = 0
        list = []
        for head in range(len(s)):
            if len(list) > max_length:
                max_length = len(list)
            list = [s[head]]
            for end in range(head+1, len(s)):
                if s[end] in list:
                    break
                else:
                    list.append(s[end])
        return length
    def lengthOfLongestSubstring(s):


s ='pwwkew'
print(Solution.lengthOfLongestSubstring(Solution, s))
