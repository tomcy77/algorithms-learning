
# 1 -> 2 -> 3 -> 4 -> None

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.prev = None

def create(input):
    created = []
    for element in input:
        created.append(Node(element))
    for i in range(len(created)):
        if i == 0:
            created[i].next = created[i+1]
        elif i == len(created) - 1:
            created[i].prev = created[i-1]
        else:
            created[i].next = created[i+1]
            created[i].prev = created[i-1]
    return created[0]
head = create([1, 2, 3, 4, 5, 6])
print(head.data)
print(head.next.data)
print(head.next.prev.data)
