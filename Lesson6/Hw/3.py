# 3. Longest Substring Without Repeating Characters
# Given a string, find the length of the longest substring without repeating
# characters.
#
# Example 1:
#
# Input: "abcabcbb"
# Output: 3
# Explanation: The answer is "abc", with the length of 3.
# Example 2:
#
# Input: "bbbbb"
# Output: 1
# Explanation: The answer is "b", with the length of 1.
# Example 3:
#
# Input: "pwwkew"
# Output: 3
# Explanation: The answer is "wke", with the length of 3.
#              Note that the answer must be a substring, "pwke" is a subsequence
#              and not a substring.
 "pwwkew"
 dic = ["p"->0, "w" -> 1]
class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        mark = {}
        max_length = 0
        start = 0
        for i, c in enumerate(s):
            if c not in mark:
                max_length = max(max_length, i - start + 1)
            else:
                for key in range(start, mark[c]):
                    del mark[s[key]]
                start = mark[c] + 1
            mark[c] = i
        return max_length

print(Solution().lengthOfLongestSubstring("tmmzuxt"))
