# 19. Remove Nth Node From End of List
#
# Given a linked list, remove the n-th node from the end of list and return
# its head.
#
# Example:
#
# Given linked list: 1->2->3->4->5, and n = 2.
#
# After removing the second node from the end, the linked list becomes
# 1->2->3->5.
# Note:
# Given n will always be valid.

class ListNode:
    def __init__(self, data):
        self.val = data
        self.next = None
def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        # put linked list in a list to measure the length of linked list
        listNode = []
        while head:
            listNode.append(head)
            head = head.next
        #when n is the first node of linked list
        if n == len(listNode):
            return listNode[0].next
        #other cases, find the node before n and link it to the node next to n
        prev = listNode[len(listNode) - n - 1]
        prev.next = prev.next.next
        return(listNode[0])

# 1 -> 2 -> 3 -> 4 -> 5
# 2 => 1 -> 2 -> 3 -> 5
# find_end = 1 -> 2 -> 3 -> 4 -> 5
# target x -> 1 -> 2 -> 3
# count = 2 -> 1 -> 0
# target.next = target.next.next

one = ListNode(1)
print(one.val)
print(one.next)
