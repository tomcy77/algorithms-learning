# One way to improve the RANDOMIZED-QUICKSORT procedure is to partition around a
# pivot that is chosen more carefully than by picking a random element from the
# subarray. One common approach is the median-of-3 method: choose the pivot as
# the median (middle element) of a set of 3 elements randomly selected from the
# subarray.

def median_3_quick_sort(input):
    pass
