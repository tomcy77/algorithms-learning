1. Background Analysis
  1. What is the highest score wine?
  1. What country produce the largest number of wines?
  1. What word is used most frequently in the description (excule "is", "the" ...)
  1. Which winery produce the largest number of wines?
  1. Plot the score trend for each year for the most productive winery (take average)
  1. For the taster who review the largest number wines, analyze the perference
  of the person (For example, what kind of wine he will give the highest score)
  1. What is the mean, median and variance of the scores per winery
  1. Does price have any relationship with the score?
