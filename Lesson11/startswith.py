import os
filenames = os.listdir('.')
print(filenames)
md = [name for name in filenames if name.endswith('.md')]
print(md)
# when multiple, it has to be a tuple
py = [name for name in filenames if name.endswith(('.py', '.ipynb'))]
print(py)

# another way: slice
filename = 'spam.txt'
print(filename[-4:] == '.txt')
url = 'http://www.python.org'
print(url[:5] == 'http:' or url[:6] == 'https:' or url[:4] == 'ftp:')

# or regular expression
import re
url = 'http://www.python.org'
print(re.match('http:|https:|ftp:', url))
