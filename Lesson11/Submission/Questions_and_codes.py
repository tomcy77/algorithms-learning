# 1. Background Analysis
#   1. What is the highest score wine?
#
import pandas as pd
data = pd.read_csv("../Hw/winemag-data-130k-v2.csv")
# pd.set_option('display.max_colwidth', None)
# max_score = data.loc[:,'points'].max()
# print(data.loc[data.loc[:,'points'] == max_score, 'title'])
#
#   2. What country produce the largest number of wines?
#
# a = data.groupby('country').size()
# print(a)
######
# data['country'].mode()
#
#   3. What word is used most frequently in the description (excule "is", "the" ...)
#
# pd.set_option('display.max_rows', 200)
# pd.set_option('display.min_rows', 200)
# b = data.loc[:, 'description'].str.split(expand=True).stack().value_counts()
# print(b[:200])
#
#   4. Which winery produce the largest number of wines?
#
# c = data.groupby('winery').size().idxmax()
# ######
# print(data['winery'].mode())
#
#   5. Plot the score trend for the most productive winery (take average)
# import re
import matplotlib.pyplot as plt
# selected = data.loc[data.loc[:, 'winery'] == 'Wines & Winemakers', :]
# x = []
# y = []
# for index, row in selected.iterrows():
#     title = row['title']
#     year = re.findall(r'[1-3][0-9]{3}', title)
#     if not year:
#         continue
#     x.append(year[0])
#     y.append(row['points'])
# pair = list(zip(x, y))
# pair = sorted(pair)
# print(pair)
# plt.scatter([i[0] for i in pair], [i[1] for i in pair])
# plt.show()
#   6. For the taster who review the largest number wines, analyze the perference
#   of the person (For example, what kind of wine he will give the highest score)
#
#   7. What is the mean, median and variance of the scores
#
# average = data.loc[:, 'points'].mean()
# print(average)
# median = data.loc[:, 'points'].median()
# print(median)
# variance = data.loc[:, 'points'].var()
# print(variance)
#
#   8. Does price have any relationship with the score?
#
x = data.loc[:, 'points']
y = data.loc[:, 'price']
print(type(y))
# plt.scatter(x, y)
# plt.show()
