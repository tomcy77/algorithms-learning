import pandas as pd

#     user_name(string) col2 ... coln
# r1  "yichi"
# r2  "yichi2"
# ...
# rn
# read
data = pd.read_csv("nba.csv")
# print head
# print(data.head)
# type
# print(data.dtypes)
# loc
# print(data.loc[0, 'Name'])
# all row for a column
# print(data.loc[:, 'Name'])
# # all column in a row
# print(data.loc[0, :])
# filter
# print(data.loc[data.loc[:, 'Age'] == 30, ('Name', 'Age')])
# max age
# print(data.loc[:, 'Age'].max())
# group by
# g = data.groupby('Age')
# print(g)
# for age, data in g:
#     print(age)
#     print(type(data))
#     print(data)
