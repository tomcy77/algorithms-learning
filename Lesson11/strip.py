# Whitespace stripping
# s = ' hello world \n'
# print(s.strip())
# print(s.lstrip())
# print(s.rstrip())
# Character stripping
# t = '-----hello====='
# print(t.lstrip('-'))
# print(t.strip('-='))
#
# it won't remove any space in the middle
s = ' hello     world \n'
s = s.strip()
print(s)
# use replace to remove middle spaces
print(s.replace(' ', ''))
# or
import re
print(re.sub('\s+', '', s))
