import matplotlib.pyplot as plt
import pandas as pd

# read
data = pd.read_csv("nba.csv")

# plot age vs salary
x = data.loc[:, 'Age']
y = data.loc[:, 'Salary']
plt.scatter(x, y)
plt.show()
