import fnmatch
import os

print("Print all files end with .py")
for file in os.listdir('.'):
    if fnmatch.fnmatch(file, '*.py'):
        print(file)
print("Print all files end with ?match.py")
for file in os.listdir('.'):
    if fnmatch.fnmatch(file, '?match.py'):
        print(file)
