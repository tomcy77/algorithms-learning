# Lesson 11

#### Any question from last lesson?
String manipulation

## String
1. startswith() and endswith()
1. [fnmatch.fnmatch()](https://docs.python.org/3/library/fnmatch.html): unix style matching
  1. *: matches everything
  1. ?: matches any single character
  1. [seq]: matches any character in seq
  1. [!seq]: matches any character not in seq
1. str.replace() vs re.sub()
1. greedy matching vs non-greedy
1. str.strip()

## File and text handling
1. [pandas](https://pandas.pydata.org/)
  1. read file: [read_csv](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html)
  1. filter, specify row and column, max
1. [matplotlib](https://matplotlib.org/)
  1. scatter
## Hw
