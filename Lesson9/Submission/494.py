# 494. Target Sum
# You are given a list of non-negative integers, a1, a2, ..., an, and a target, S.
# Now you have 2 symbols + and -. For each integer, you should choose one
# from + and - as its new symbol.
#
# Find out how many ways to assign symbols to make sum of integers equal to target S.
#
# Example 1:
# Input: nums is [1, 1, 1, 1, 1], S is 3.
# Output: 5
# Explanation:
#
# -1+1+1+1+1 = 3
# +1-1+1+1+1 = 3
# +1+1-1+1+1 = 3
# +1+1+1-1+1 = 3
# +1+1+1+1-1 = 3
#
# There are 5 ways to assign symbols to make the sum of nums be target 3.
#
# Constraints:
#
# The length of the given array is positive and will not exceed 20.
# The sum of elements in the given array will not exceed 1000.
# Your output answer is guaranteed to be fitted in a 32-bit integer.

#V1
class Solution:
    def findTargetSumWays(self, nums: List[int], S: int) -> int:
        n = len(nums)
        memo =[]
        for i in range(n+1):
            new_list = [0 for _ in range(2**i)]
            memo.append(new_list)
        memo[0][0] = 0 #memo[depth][number from left to right] = sum
        for i in range(1,n+1):
            for j in range(len(memo[i])):
                if j % 2 == 0:
                    memo[i][j] = memo[i-1][j//2] + nums[i-1]
                if j % 2 != 0:
                    memo[i][j] = memo[i-1][j//2] - nums[i-1]
        return memo[n].count(S)

#time limited exceed

#V2
class Solution:
    def findTargetSumWays(self, nums: List[int], S: int) -> int:
        n = len(nums)
        t = sum(nums)
        if t < S:
            return 0
        memo = [[0 for _ in range(2*t+1)]for _ in range(n+1)]
        memo[0][0] = 1#memo[xth_nums][possible_sum] = ways
        for i in range(n):
            for j in range(2*t+1):
                if memo[i][j] != 0:
                    if j > t:
                        real_index = -(2* t+ 1- j)
                        memo[i+1][real_index+nums[i]] += memo[i][real_index]
                        memo[i+1][real_index-nums[i]] += memo[i][real_index]
                    else:
                        memo[i+1][j+nums[i]] += memo[i][j]
                        memo[i+1][j-nums[i]] += memo[i][j]
        return memo[n][S]

#leetcode accepted
