# list
# mylist = [1, 2, 3]
# for i in mylist:
#     print(i)

# generators: you can only iterate over once
mygenerator = (x*x for x in range(3))
for i in mygenerator:
    print(i)
# become empty now
for i in mygenerator:
    print(i)

# generator is a object
def createGenerator():
    mylist = range(3)
    for i in mylist:
        yield i*i
# mygenerator = createGenerator()
# print(mygenerator)
# for i in mygenerator:
#     print(i)
import time
# start_time = time.time()
# longlist = [x for x in range(10000000)]
# print(time.time() - start_time)
# print(longlist[0])

start_time = time.time()
long_generator = (x for x in range(10000000))
print(time.time() - start_time)
print(long_generator)
