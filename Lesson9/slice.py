record = '....................100 .......513.25 ..........'
cost = int(record[20:23]) * float(record[31:37])

# better
SHARES = slice(20, 23)
PRICE = slice(31, 37)
cost = int(record[SHARES]) * float(record[PRICE])
