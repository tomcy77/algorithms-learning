def dedupe(items):
    seen = set()
    for item in items:
        if item not in seen:
            yield item
            seen.add(item)
import time
import random
a = [random.randint(-10000, 10000)for i in range(10000000)]
mydedupe = dedupe(a)
# return a generator

# start_time = time.time()
print(mydedupe)
# print(time.time() - start_time)
# make it a list
start_time = time.time()
print(next(mydedupe))
print(time.time() - start_time)
