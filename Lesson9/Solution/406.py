class Solution:
    def reconstructQueue(self, people):
        # people = sorted(people, key=lambda s: s[1])
        return self.recursion([], people)

    def recursion(self, cur, left):
        if not left:
            return cur
        for i, person in enumerate(left):
            height, higher = person
            counter = 0
            for pre in cur:
                if pre[0] >= height:
                    counter += 1
            if counter != higher:
                continue
            result = self.recursion(cur + [person], left[:i] + left[i+1:])
            if result:
                return result

people = [[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]
print(Solution().reconstructQueue(people))
