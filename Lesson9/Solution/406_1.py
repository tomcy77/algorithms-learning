class Solution:
    def reconstructQueue(self, people):
        result = [[] for _ in people]
        self.recursion(result, people)
        return result

    def recursion(self, result, left):
        if not left:
            return
        shortest = min([i[0] for i in left])
        max_higher, max_index = -1, -1
        for i, element in enumerate(left):
            if element[0] == shortest:
                if element[1] > max_higher:
                    max_higher = element[1]
                    max_index = i

        height, higher = left[max_index]
        counter = 0
        for j in range(len(result)):
            if result[j] != []:
                continue
            if counter == higher:
                result[j] = left[max_index]
                break
            if result[j] == []:
                counter += 1
        del left[max_index]
        self.recursion(result, left)



people = [[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]
print(Solution().reconstructQueue(people))
