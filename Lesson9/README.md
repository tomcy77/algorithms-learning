# Lesson 9
Generator and index

#### Any question from last lesson?

## Data struction and functions
1. yield
  1. list vs generator
  1. generator init is not running (it will keep the current status and wait until next call)
1. Remove duplicated elements but keep order
  1. set (not keep the order)
  1. dedupe function
  1. dedupe with nonhashable
1. index with slice
  1. [index](https://docs.python.org/2.3/whatsnew/section-slices.html)
    1. L[start: end: step]
    1. L = range(10)
    1. L[::2]
    1. L[::-1]
    1. L[1:3] = [4, 5]
    1. a[::2] = [0, 1, 2, 3] # ValueError
  1. give index a name for easier understanding
1. groupby
  1. [itertools.groupby()](https://docs.python.org/3/library/itertools.html#itertools.groupby)
  1. example: groupby.py
1. [filter](https://docs.python.org/3/library/functions.html#filter)
  1. list
  1. generator
  1. filter
  1. compress
1. derived dictionary: create a subset of a dictionary

## Hw
