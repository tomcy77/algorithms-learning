from datetime import datetime
text = '2020-10-01'
y = datetime.strptime(text, '%Y-%m-%d')
z = datetime.now()
diff = z - y
print(diff)

# print date by format
nice_z = datetime.strftime(z, '%A %B %d, %Y')
print(nice_z)

# parse without library, 7 times faster
def parse_ymd(s):
    year_s, mon_s, day_s = s.split('-')
    return datetime(int(year_s), int(mon_s), int(day_s))

print(parse_ymd(text))
