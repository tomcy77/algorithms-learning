# Given a string containing only three types of characters: '(', ')' and '*',
#  write a function to check whether this string is valid. We define the
#  validity of a string by these rules:
#
# Any left parenthesis '(' must have a corresponding right parenthesis ')'.
# Any right parenthesis ')' must have a corresponding left parenthesis '('.
# Left parenthesis '(' must go before the corresponding right parenthesis ')'.
# '*' could be treated as a single right parenthesis ')' or a single left
# parenthesis '(' or an empty string.
# An empty string is also valid.
# Example 1:
# Input: "()"
# Output: True
# Example 2:
# Input: "(*)"
# Output: True
# Example 3:
# Input: "(*))"
# Output: True
# Note:
# The string size will be in the range [1, 100].

class Solution:
    def checkValidString(self, s: str) -> bool:
        counter = 0
        for i in range(len(s)):
            if s[i] == '(' or s[i] == '*':
                counter += 1
            else:
                counter -= 1
            # continuesly check the balance
            if counter < 0:
                return False
        if counter == 0:
            return True
        counter = 0
        for j in range(len(s)-1, -1, -1):
            if s[j] == ')' or s[j] == '*':
                counter += 1
            if s[j] == '(':
                counter -= 1
            if counter < 0:
                return False
        return True


#leetcode accepted
# 1. naive: 3^100 * 100
# 2. (*)()
