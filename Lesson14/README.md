# Lesson 14
Date and Iterator

#### Any question from last lesson?
Numpy and date

## Date
1. parse date from string: datetime.strptime()
1. date with timezone: pytz
1. all time zone: [list](https://stackoverflow.com/questions/13866926/is-there-a-list-of-pytz-timezones)

## Iterator and generator
1. __example__
1. [iterator](https://www.programiz.com/python-programming/iterator) mechenism
  1. \_\_iter__() and \_\_next__() (iterator protocol)
  1. iter() invokes \_\_iter__
  1. what are iterator? string, list, tuple, set, dict
  1. what are not itertator? object, integer, float, functions
  1. use iter to create iterator, use next to return next element
1. custom object with Iterator
  1. define \_\_iter__()

## Hw
1. [Twine](https://twinery.org/)
