from datetime import datetime
from pytz import timezone
import pytz
from datetime import timedelta

# d = datetime(2020, 10, 1, 20, 30, 0)
# print(d)
#
pac = timezone('US/Pacific')
# loc_d = pac.localize(d)
# print(loc_d)
#
# # convert to Japan time
# jap_d = loc_d.astimezone(timezone('Asia/Tokyo'))
# print(jap_d)
# #
# # problem with daylight saving time
# d = datetime(2013, 3, 10, 1, 45)
# loc_d = pac.localize(d)
# print(loc_d)
#
# # give you wrong answer since the daylight saving time
# later = loc_d + timedelta(minutes=30)
# print(later)
#
# # recommended solution: use normalize
# later = pac.normalize(loc_d + timedelta(minutes=30))
# print(later)
#
# recommended solution 2: all arithmetic calculation use utc timezone
loc_d = datetime.now()
print(loc_d)
# convert to utc first
utc_d = loc_d.astimezone(pytz.utc)
print(utc_d)

# use utc to calculate
later_utc = utc_d + timedelta(minutes=30)
# convert to disired timezone
print(later_utc.astimezone(pac))
