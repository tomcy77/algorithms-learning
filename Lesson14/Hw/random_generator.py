# https://en.wikipedia.org/wiki/Linear_congruential_generator
# 1. implement a random generator/iterator following the above algorithm
# 2. compare it with buildin random algorithm and prove it's correct

# m => 0 - 10 roll 100 times
# 0 -> 10
# 1 -> 10
# 2 -> 10
# ...
# 10 -> 10

# 3. use it to create a dice which has
# 0.3 probablity of -1
# 0.35 prob of 0
# 0.2 prob of 1
# 0.15 prob of 2
# show the expectation of the dice is 0.2 by simulation
# plot the graph with x = value and y = count
import math
from collections import Counter

class Random_n:
    def __init__(self, seed, n):
        self.__a = 1103515245
        self.__c = 12345
        self.__m = pow(2, 31)
        self.seed = seed
        self.n = n

    def __iter__(self):
        self.counter = 0
        return self

    def __next__(self):
        if self.counter < self.n:
            self.counter += 1
            return self.generate()
        raise StopIteration

    def generate(self):
        next_random = (self.__a * self.seed + self.__c) % self.__m
        self.seed = next_random
        return next_random % 10 + 1

random_n_object = Random_n(100, 5)
iter(random_n_object)
print(next(random_n_object))
print(next(random_n_object))
print(next(random_n_object))
print(next(random_n_object))
print(next(random_n_object))
print(next(random_n_object))


class Random:
    def __init__(self, seed):
        self.__a = 1103515245
        self.__c = 12345
        self.__m = pow(2, 31)
        self.seed = seed

    def __iter__(self):
        yield self.generate()

    def __next__(self):
        pass

    def generate(self):
        next_random = (self.__a * self.seed + self.__c) % self.__m
        self.seed = next_random
        return next_random % 10 + 1

    def generate_n(self, n):
        return [self.generate() for _ in range(n)]
# random_object = Random(1)
# result = random_object.generate_n(1000000)
# counter = Counter()
# for i in result:
#     counter[i] += 1
# print(counter)

class Dice:
    def __init__(self, seed):
        pass

    def __iter__(self):
        pass

    def __next__(self):
        pass
