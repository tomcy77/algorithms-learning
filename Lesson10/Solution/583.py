# 583. Delete Operation for Two Strings
# Given two words word1 and word2, find the minimum number of steps required to
# make word1 and word2 the same, where in each step you can delete one character
# in either string.
#
# Example 1:
# Input: "sea", "eat"
# Output: 2
# Explanation: You need one step to make "sea" to "ea" and another step to make "eat" to "ea".
# Note:
# The length of given words won't exceed 500.
# Characters in given words can only be lower-case letters.
# dp
#          s  e  a
#    0, 0, 1, 2, 3, ..., m (A)
#    0  0, 0, 0, 0, 0
# e  1  0, 0, 1, 1
# a  2  0, 0, 1, 2
# t  3  0, 0, 1, 2
#    4
#   ...
# n
# (B)
# dp[i][j] = if A[j] == B[i] => dp[i-1][j-1] + 1
# else => max(dp[i-1][j], dp[i][j-1])

class Solution:
    def minDistance(self, word1: str, word2: str) -> int:
        dp = [[0 for _ in range(len(word1)+1)] for _ in range(len(word2)+1)]
        for i in range(len(word1)):
            for j in range(len(word2)):
                if word1[i] == word2[j]:
                    dp[j+1][i+1] = dp[j][i] + 1
                else:
                    dp[j+1][i+1] = max(dp[j][i+1], dp[j+1][i])
        return len(word1) + len(word2) - 2 * dp[-1][-1]
print(Solution().minDistance("eat", "sea"))
