class Solution:
    def isPathCrossing(self, path: str) -> bool:
        s = set()
        cur = (0, 0)
        for p in path:
            if cur in s:
                return True
            s.add(cur)
            if p == 'N':
                cur = (cur[0], cur[1] + 1)
            elif p == 'S':
                cur = (cur[0], cur[1] - 1)
            elif p == 'E':
                cur = (cur[0] + 1, cur[1])
            else:
                cur = (cur[0] - 1, cur[1])
        if cur in s: ## O(1)
            return True
        return False
print(Solution().isPathCrossing("NESWW"))
