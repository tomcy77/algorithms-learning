from collections import deque

class Solution:
    def backspaceCompare(self, S: str, T: str) -> bool:
        return self.machine(S) == self.machine(T)

    def machine(self, input):
        d = deque()
        for c in input:
            if c == '#' and len(d) > 0:
                d.pop()
            if c != '#':
                d.append(c)
        return "".join(list(d))

print(Solution().backspaceCompare("ab##", "a#c#"))
