# Lesson 10

#### Any question from last lesson?
Generator and index

## Data struction and functions
1. collections.namedtuple(): create tuple with name that you can access
1. collections.ChainMap: create a single and updatable view from multiple maps(dict)

## String and seperators
1. regular expression [re library](https://docs.python.org/3/library/re.html)
  1. .: matches any character except a newline
  1. ^: matches the start of the string
  1. $: matches the end of the string or just before the newline at the end of the string
  1. \*: causes the resulting RE to match 0 or more repetitions of the preceding RE. ab\* will match ‘a’, ‘ab’, or ‘a’ followed by any number of ‘b’s.
  1. +: causes the resulting RE to match 1 or more repetitions of the preceding RE. ab+ will match ‘a’ followed by any non-zero number of ‘b’s; it will not match just ‘a’.
  1. ?: causes the resulting RE to match 0 or 1 repetitions of the preceding RE. ab? will match either ‘a’ or ‘ab’.
  1. []: used to indicate a set of characters. Example: [amk] will match 'a', 'm', or 'k'.
  1. (...): matches whatever regular expression is inside the parentheses, and indicates the start and end of a group; the contents of a group can be retrieved after a match has been performed
  1. \s: matches Unicode whitespace characters
1. string split: string.split() vs re.split()
## Hw
