from collections import namedtuple
Subscriber = namedtuple('Subscriber', ['addr', 'joined'])

sub = Subscriber('jonesy@example.com', '2012-10-19')
print(sub)
class Sub:
    def __init__(self, addr, joined):
        self.addr = addr
        self.joined = joined
    def clean_data():
        pass

subb = Sub('jonesy@example.com', '2012-10-19')
Subscriber(addr='jonesy@example.com', joined='2012-10-19')
print(sub.addr)
print(sub.joined)
# similar to class but its a tuple
print(len(sub))
addr, joined = sub
print(addr)
print(joined)

# usage example
def compute_cost(records):
    total = 0.0
    for rec in records:
        total += rec[1] * rec[2]
    return total

# => with naming
from collections import namedtuple
Stock = namedtuple('Stock', ['name', 'shares', 'price'])
def compute_cost(records):
    total = 0.0
    for rec in records:
        s = Stock(*rec)
        total += s.shares * s.price
    return total
