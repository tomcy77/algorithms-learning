# string.split(separator, maxsplit)
# txt = "welcome to the jungle"
# x = txt.split()
# print(x)

# specify seperator
# txt = "hello, my name is Peter, I am 26 years old"
# x = txt.split(", ")
# print(x)

# re.split()
import re
line = 'asdf fjdk; afed, fjek,asdf, foo'
# x = re.split(r'[;,\s]\s*', line)
# print(x)
#
# ()
fields = re.split(r'(;|,|\s)\s*', line)
print(fields)
