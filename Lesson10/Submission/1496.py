# 1496. Path Crossing
# Given a string path, where path[i] = 'N', 'S', 'E' or 'W', each representing
# moving one unit north, south, east, or west, respectively. You start at the
# origin (0, 0) on a 2D plane and walk on the path specified by path.
#
# Return True if the path crosses itself at any point, that is, if at any time
# you are on a location you've previously visited. Return False otherwise.

class Solution:
    def isPathCrossing(self, path: str) -> bool:
        history = [[0, 0]]
        for i in range(len(path)):
            if path[i] == 'N':
                position = [history[i][0], history[i][1] + 1]
            elif path[i] == 'E':
                position = [history[i][0] + 1, history[i][1]]
            elif path[i] =='S':
                position = [history[i][0], history[i][1] - 1]
            elif path[i] == 'W':
                position = [history[i][0] - 1, history[i][1]]
            if position in history: # O(N)
                return True
            else:
                history.append(position)
        return False

#leetcode accepted
