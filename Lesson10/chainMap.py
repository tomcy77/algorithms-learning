a = {'x': 1, 'z': 3 }
b = {'y': 2, 'z': 4 }

from collections import ChainMap
c = ChainMap(a,b)
# print(c['x']) # Outputs 1 (from a)
# print(c['y']) # Outputs 2 (from b)
# # if duplicated => return the first one
# print(c['z']) # Outputs 3 (from a)

# update
# c['z'] = 10
# print(a)
# c['w'] = 40
# print(a)
# del c['x']
# print(c)
# # illegal (not exist in first map)
# del c['y']

# option 2
merged = b
merged.update(a)
print(merged)
# update won't take effect in dict a or b
a['x'] = 13
print(merged['x'])
