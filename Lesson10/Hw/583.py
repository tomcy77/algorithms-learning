# 583. Delete Operation for Two Strings
# Given two words word1 and word2, find the minimum number of steps required to
# make word1 and word2 the same, where in each step you can delete one character
# in either string.
#
# Example 1:
# Input: "sea", "eat"
# Output: 2
# Explanation: You need one step to make "sea" to "ea" and another step to make "eat" to "ea".
# Note:
# The length of given words won't exceed 500.
# Characters in given words can only be lower-case letters.
import collections

class Solution:
    def minDistance(self, word1: str, word2: str) -> int:
        d1 = self.createSubStringSet(word1)
        d2 = self.createSubStringSet(word2)
        maxSubStringLen = 0
        for w in d1:
            if w in d2:
                maxSubStringLen = max(maxSubStringLen, len(w))
        return len(word1) + len(word2) - 2 * maxSubStringLen

    def createSubStringSet(self, word):
        result = set()
        self.recursion(word, result)
        return result

    def recursion(self, word, result):
        if not word:
            return
        result.add(word)
        for i in range(len(word)):
            self.recursion(word[:i] + word[i+1:], result)
        return

print(Solution().minDistance("eat", "sea"))
