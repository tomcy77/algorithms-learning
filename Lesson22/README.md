# Lesson 22

#### Any question from last lesson?

## Classes
1. calling a method on a parent class
  1. super()
1. extending a property in a subclass
1. creating a new kind of class or instance attribute
  1. another way of implementing property
  1. descriptor: [\_\_get__ and \_\_set__](https://stackoverflow.com/questions/3798835/understanding-get-and-set-and-python-descriptors)
  1. [official doc](https://docs.python.org/3/howto/descriptor.html#simple-example-a-descriptor-that-returns-a-constant)
  1. protocol:
    1. descr.__get__(self, obj, type=None) -> value
    1. descr.__set__(self, obj, value) -> None
    1. descr.__delete__(self, obj) -> None

## Hw
