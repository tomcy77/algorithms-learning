# Simple example: A descriptor that returns a constant
class Ten:
    def __get__(self, obj, objtype=None):
        return 10

class A:
    x = 5                       # Regular class attribute
    y = Ten()                   # Descriptor instance

    def __init__(self, w, z):
        self.w = w
        self.z = z

a = A(1, 2)
print(a.x) # return 5
print(a.y) # return 10
#
# a more complex example
import os

class DirectorySize:
    def __get__(self, obj, objtype=None):
        return len(os.listdir(obj.dirname))

class Directory:
    size = DirectorySize()              # Descriptor instance

    def __init__(self, dirname):
        self.dirname = dirname          # Regular instance attribute

s = Directory('../Lesson21')
g = Directory('../Lesson22')
print(s.size)
create a new file in Lesson 22
print(g.size) # plus one now => the value is calculated in runtime

# Managed attributes
import logging

logging.basicConfig(level=logging.INFO)

class LoggedAgeAccess:
    def __get__(self, obj, objtype=None):
        logging.info('Accessing %r giving %r', 'age', obj._age)
        return obj._age

    def __set__(self, obj, value):
        logging.info('Updating %r to %r', 'age', value)
        obj._age = value

class Person:
    age = LoggedAgeAccess()             # Descriptor instance

    def __init__(self, name, age):
        self.name = name                # Regular instance attribute
        self.age = age                  # Calls __set__()

    def birthday(self):
        self.age += 1                   # Calls both __get__() and __set__()
#
mary = Person('Mary M', 30)
dave = Person('David D', 40)
print(vars(mary)) # The actual data is in a private attribute
print(vars(dave))
print(mary.age) # Access the data and log the lookup
mary.birthday() # Updates are logged as well
print(mary.age)
#
print(dave.name) # Regular attribute lookup isn't logged
print(dave.age) # Only the managed attribute is logged

# you can do input validation also, for example
# Descriptor attribute for an integer type-checked attribute
class Integer:
    def __init__(self, name):
        self.name = name

    def __get__(self, instance, cls):
        if instance is None:
            raise ValueError("123")
        else:
            return instance.__dict__[self.name]

    def __set__(self, instance, value):
        if not isinstance(value, int):
            raise TypeError('Expected an int')
        instance.__dict__[self.name] = value

    def __delete__(self, instance):
        del instance.__dict__[self.name]

class Point:
    x = Integer('x')
    y = Integer('y')

    def __init__(self, x, y):
        self.x = x
        self.y = y

p = Point(2, 3)
print(p.x)
print(p.y)

p.y = 5
p.x = 2.3 # TypeError: Expected an int

# decriptor can only define in class level
# Does NOT work
class Point:
    def __init__(self, x, y):
        self.x = Integer('x') # No! Must be a class variable
        self.y = Integer('y')
        self.x = x
        self.y = y
