# class Animal:
#     def bark(self):
#         print('Animal.bark')
#
# class Dog(Animal):
#     def bark(self):
#         print('Dog.bark')
#         super().bark()  # Call parent spam()
#
# # using in init
# class Animal:
#     def __init__(self):
#         self.age = 10
#
# class Dog(Animal):
#     def __init__(self):
#         super().__init__()
#         self.color = 'blue'
# dog = Dog()
# print(dog.age)
# print(dog.color)
#
# # example of calling Base derictly
# class Base:
#     def __init__(self):
#         print('Base.__init__')
#
# class A(Base):
#     def __init__(self):
#         Base.__init__(self)
#         print('A.__init__')
# a = A()

# example of calling Base twice
# class Base:
#     def __init__(self):
#         print('Base.__init__')
#
# class A(Base):
#     def __init__(self):
#         Base.__init__(self)
#         print('A.__init__')
#
# class B(Base):
#     def __init__(self):
#         Base.__init__(self)
#         print('B.__init__')
#
# class C(A,B):
#     def __init__(self):
#         A.__init__(self)
#         B.__init__(self)
#         print('C.__init__')
# c = C()
#
# correct way
# class Base:
#     def __init__(self):
#         print('Base.__init__')
#
# class A(Base):
#     def __init__(self):
#         super().__init__()
#         print('A.__init__')
#
# class B(Base):
#     def __init__(self):
#         super().__init__()
#         print('B.__init__')
#
# class C(A,B):
#     def __init__(self):
#         super().__init__()  # Only one call to super() here
#         print('C.__init__')
# c = C()

# MRO list
# print(C.__mro__)
#
# a more complex but error example
class A:
    def spam(self):
        print('A.spam')
        super().spam()
a = A()
# a.spam() # give you exception (no super)

class B:
    def spam(self):
        print('B.spam')

class C(A,B):
    pass
c = C()
c.spam()
#
# MRO list
print(C.__mro__)
