# Read the entire file as a single string
with open('example.txt', 'rt') as f:
    data = f.read()
    print(data)

# Iterate over the lines of the file
with open('example.txt', 'rt') as f:
    for line in f:
        # process line
        print(line)

# you need to close the file manually.
f = open('example.txt', 'rt')
data = f.read()
f.close()
