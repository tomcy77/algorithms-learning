# Lesson 17

#### Any question from last lesson?
iterator

## Open discussion
1. A more long term project
  1. web crawler
  1. game
  1. website
1. card game calculator:
  1. user can use the website to calculate the prob of starting cards
  1. user categorized cards
  1. user input:
    1. deck total
    1. starting cards number
    1. category of cards
    1. number of each category
    1. calculate

## iterator
1. Create data processing pipeline
1. Use yield from to implement flatten function
1. Merge sorted iterators: [heapq.merge()](https://docs.python.org/2/library/heapq.html#heapq.merge)

## Files and I/O
1. read file: [open()](https://docs.python.org/3/tutorial/inputoutput.html#reading-and-writing-files)
  1. r: read only
  1. w: write only
  1. r+: read and writing
  1. a: appending
  1. normally in text mode, can be other mode like binary mode(b)
  1. encoding: utf-8, ascii, utf-16 and latin-1
1. print to file
1. read and write binary file
  1. rb or wb to open the files
1. only write if not existing: xt or xb
1. simulate a doc without creating a real file: io.StringIO() and io.BytesIO()
1. read and write compressed files like gzip or bz2
  1. [gzip.open](https://docs.python.org/3/library/gzip.html) and [bz2.open](https://docs.python.org/3/library/bz2.html)
  1. use compresslevel to specify the level of compression.

## Hw
