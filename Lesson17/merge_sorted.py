import heapq

a = [1, 4, 7, 10]
b = [2, 5, 6, 11]
for c in heapq.merge(a, b):
    print(c)

# same as
x = y = 0
while x < len(a) or y < len(b):
    if x == len(a):
        print(b[y])
        y += 1
    elif y == len(b):
        print(a[x])
        x += 1
    elif a[x] < b[y]:
        print(a[x])
        x += 1
    else:
        print(b[y])
        y += 1
