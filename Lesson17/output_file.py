# with open('example.txt', 'wt') as f:
#     print('Hello World!', file=f)

# the file needs to be opened as text mode
# binary mode will give you exception

print('ACME', 50, 91.5)
# ACME 50 91.5
print('ACME', 50, 91.5, sep=',')
# ACME,50,91.5
print('ACME', 50, 91.5, sep=',', end='!!\n')
# ACME,50,91.5!!

# use end to avoid new line
for i in range(5):
    print(i)

for i in range(5):
    print(i, end=' ')

# can achieve by join
print(','.join(('ACME','50','91.5')))

# but you need to custom convert it to string if any elemet is not string
row = ('ACME', 50, 91.5)
print(','.join(row))
# Traceback (most recent call last):
#     File "<stdin>", line 1, in <module>
# TypeError: sequence item 1: expected str instance, int found
print(','.join(str(x) for x in row))
# ACME,50,91.5

# also
print(*row, sep=',')
