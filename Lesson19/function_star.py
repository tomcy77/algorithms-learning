def avg(first, *rest):
    print(rest) ## rest is a tuple.
    return (first + sum(rest)) / (1 + len(rest))

def avg2(*all):
    return sum(all) / len(all)

print(avg2(1, 2))
# 1.5
print(avg2(1, 2, 3, 4))
# 2.5
#
import html
# use ** as a parameter dictionary
def make_element(name, value, **attrs):
    keyvals = [' {}="{}"'.format(item[0], item[1]) for item in attrs.items()]
    attr_str = ''.join(keyvals)
    element = '<{name}{attrs}>{value}</{name}>'.format(
                name=name,
                attrs=attr_str,
                value=html.escape(value))
    return element

# Example
# Creates '<item size="large" quantity="6">Albatross</item>'
print(make_element('item', 'Albatross', size='large', quantity=6))

# Creates '<p>&lt;spam&gt;</p>'
print(make_element('p', '<spam>'))

# use * and ** at the same time
def anyargs(*args, **kwargs):
    print(args) # A tuple
    print(kwargs) # A dict
anyargs(1, 2, 3, 4, a = 1, b = 2)

# we can put parameter after *
# for example
def a(x, *args, y):
    pass

a(1, y = 3)
def b(x, *args, y, **kwargs):
    pass
