# Lesson 19
Functions

#### Any question from last lesson?

## News
1. [Guido van Rossum joined Microsoft](https://www.zdnet.com/article/guido-van-rossum-the-python-languages-founder-joins-microsoft/)

## Data Encoding and Processing
1. Some xml handling -> skip
1. binary, hex converison -> skip

# Functions
1. writing functions that accept any number of arguments: * and **
1. writing functions that only accept keyword arguments: use *
  1. example: "msg = recv(1024, False)" is worse than "msg = recv(1024, block=False)"
1. add metadata to functions: [typing](https://docs.python.org/3/library/typing.html)
  1. give developers hint about how to use the functions
  1. type checking
  1. generic type: T
1. returns multiple value from a function: return a, b, c
  1. comma creates the tuple -> pack and unpack
1. define a function with default parameter
  1. use None for list, set or dictionary

## Project
1. wtform? how to add button (in flask)
1. simulation:
  1. randomly generate hand deck
  1. repeat 100000
  1. check if match the condition (n)
  1. n / 100000
1. probability way
1. jinja2
