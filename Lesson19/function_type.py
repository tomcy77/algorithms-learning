def add(x:int, y:int) -> int:
    """ docstring"""
    return x + y

print(help(add))


from typing import List
# import typing library

def add2(x: List[int], y: List[int]) -> int:
    return sum(x) + sum(y)

print(add2([1, 2, 3], [4, 5, 6]))
# the annotation is stored in __annotations__
print(add2.__annotations__)
