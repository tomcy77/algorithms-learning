# def spam(a, b=42):
#     print(a, b)
#
# # spam(1) # Ok. a=1, b=42
# # spam(1, 2) # Ok. a=1, b=2
#
# # Using a list as a default value
# def spam2(a, b:list=None):
#     if b is None:
#         b = []
#     ...
#
# # or => bad example since [] can be changed
# def spam3(a, b=[]):
#     print(b)
#     return b
# x = spam3(1)
# print(x) # []
# x.append(99)
# x.append('Yow!')
# print(x)
# spam3(1) # Modified list gets returned!

# test value is provided or not
_no_value = object()
def spam4(a, b=_no_value):
    if b is _no_value:
        print('No b value supplied')
spam4(1) # No b value supplied
spam4(1, 2)
spam4(1, None)

# the default value is assigned when function defines
x = 42
def spam5(a, b=x):
    print(a, b)

spam5(1)
x = 23 # Has no effect
spam5(1)

# bad example
def spam6(a, b=None):
    if not b: # NO! Use 'b is None' instead
        b = []
    ...
spam(1) # OK
x = []
spam(1, x) # Silent error. x value overwritten by default
spam(1, 0) # Silent error. 0 ignored
spam(1, '') # Silent error. '' ignored
